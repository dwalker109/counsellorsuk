<?php

require 'recipe/laravel.php';

serverList('servers.yml');

set('repository', 'git@bitbucket.org:dwalker109/counsellorsuk.git');

set('copy_dirs', ['vendor', 'node_modules']);

task('deploy:npm', function () {
    run("cd {{release_path}} && npm install");
})->desc('Installing node packages');

task('deploy:elixir', function () {
    run("cd {{release_path}} && ./node_modules/gulp/bin/gulp.js --production");
})->desc('Running elixir');

task('reload:php-fpm', function () {
    run('sudo /usr/sbin/service php7.1-fpm reload');
});

task('reload:supervisor', function () {
    run('sudo /usr/sbin/service supervisor reload');
});

task('artisan:config:cache', function () {
    write('Skipping config cache for now - breaks this app!');
});

task('reload:all', [
    'reload:php-fpm',
    'reload:supervisor',
]);

before('deploy:vendors', 'deploy:copy_dirs');
after('deploy:vendors', 'deploy:npm');
after('deploy:npm', 'deploy:elixir');

after('deploy', 'reload:all');
after('rollback', 'reload:all');
