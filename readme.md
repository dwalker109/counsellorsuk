# Counsellors UK

Version 2, rewritten in 2016 in Laravel 5.2.
Upgraded to Laravel 5.3 in September 2016.

## Data import

The `oldsitedata:import` command has been removed now. 