<section>
    <div class="cms-page__solo">
        <h2>{{ $cms_page->title }}</h2>
        {!! $cms_page->body !!}
    </div>
</section>
