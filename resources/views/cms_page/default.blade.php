@extends('layouts.app')

@section('title', $cms_page->title)
@section('meta_description', $cms_page->meta_description)

@section('content')
    
<section>
    <div class="cms-page__article">
        <h2>{{ $cms_page->title }}</h2>
        {!! $cms_page->body !!}
    </div>
    
    <aside class="cms-page__search-and-signup">
        <section>
            <div class="cms-page__search">
                @include('search._form')
            </div>
            <div class="cms-page__signup">
                @include('_signup')
            </div>
        </section>
    </aside>
</section>
    
@endsection