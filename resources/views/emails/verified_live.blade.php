@extends('layouts.email')

@section('email_content')
<p><strong>{{ $user->name }}, congratulations! Your Counsellors UK profile has been
verified and is now live.</strong></p>

<p><a href="{{ route('counsellor', $user->profile->slug) }}">View your profile</a></p>

@if (!$user->profile->meta_is_subscriber)
	<p><strong>Get more out of your profile</strong></p>

	<p>Our aim is simply to create the best counsellors directory in the UK. This means
	that we are extremely pleased that you have chosen to join us!</p>

	<p>However, should you wish to take advantage of a full membership profile, here
	are some reasons:</p>

	<ul>
	    <li>Appear above free profiles in search results</li>
	    <li>Greatly expand your biography (to over 15,000 characters)</li>
	    <li>Include information on your fees and appointment scheduling</li>
	    <li>Detail any concessionary rates you offer </li>
	</ul>

	<p>Simply <a href="{{ route('profile.view') }}">log in to your account</a>
	to upgrade.</p>
@endif

@endsection
