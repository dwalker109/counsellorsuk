@extends('layouts.email')

@section('email_content')
<p><strong>{{ $user->name }}, welcome to CounsellorsUK!  You can now log into your
CounsellorsUK member account:</strong></p>

<p><a href="{{ route('profile.view') }}">{{ route('profile.view') }}</a></p>

<p>If requested, please use the email address ({{ $user->email }}) and password that you set during 
the registration process. Don’t worry if you have forgotten your password - you can also log in with 
just your email address, or use the forgotten password facility, all from the login page.</p>

<p>Please <a href="{{ route('profile.details') }}">complete your <strong>professional 
body registration number</strong> and, if applicable, <strong>BACP certificate number</strong></a>
within 3 working days so we can verify your account. You will receive a notification
email once we have done this.</p>

<p>The member account area will allow you to manage your entire profile and keep
your details up-to-date.</p>

<p><strong>Get more out of your profile</strong></p>

<p>Our aim is simply to create the best counsellors directory in the UK. This means
that we are extremely pleased that you have chosen to join us!</p>

<p>However, should you wish to take advantage of a full membership profile, here
are some reasons:</p>

<ul>
    <li>Appear above free profiles in search results</li>
    <li>Greatly expand your biography (to over 15,000 characters)</li>
    <li>Include information on your fees and appointment scheduling</li>
    <li>Detail any concessionary rates you offer </li>
</ul>

<p>Simply <a href="{{ route('profile.view') }}">log in to your account</a>
to upgrade.</p>

@include('emails._help_and_vision')
@endsection
