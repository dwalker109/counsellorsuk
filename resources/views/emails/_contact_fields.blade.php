<table>
    <tr>
        <td><strong>Name:</strong> {{ $info->name ?? 'Not provided' }}</td>
    </tr>
    <tr>
        <td><strong>Contact preference:</strong> {{ ucfirst($info->contact_preference) }}</td>
    </tr>
    <tr>
        <td><strong>Email:</strong>
            @if ($info->email)
                <a href="mailto:{{ $info->email }}" target="_blank" alias="" style="font-size:12px; line-height:18px; color:blue; text-decoration:underline;">{{ $info->email }}</a>
            @else
                Not provided
            @endif
        </td>
    </tr>
    <tr>
        <td><strong>Phone:</strong> {{ $info->phone ? phone_format($info->phone, 'GB') : 'Not provided' }}</td>
    </tr>
</table>

{!! Purifier::clean($info->message) !!}
