@extends('layouts.email')

@section('email_content')
<h2>Login to CounsellorsUK</h2>

<p>Click the link below to log in to your account at CounsellorsUK.</p>

<p><a href="{{ route('magic_link.process', $short_link) }}">{{ route('magic_link.process', $short_link) }}</a></p>

<p>If you cannot click the link, your email application is probably blocking it. 
Just copy and paste the link into your internet browser instead.</p>

<p><strong>Please note:</strong> If you did not request this magic link, there is 
no need to do anything - it can be safely ignored.</p>
@endsection