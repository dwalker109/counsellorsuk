@extends('layouts.email')

@section('email_content')
<h2>{{ $heading }}</h2>

<p>{{ $user->name }}, {{ $user->email }} -
    <a href="{{ route('admin.member.index', ['search' => $user->email]) }}">Show in admin</a>
</p>
@endsection
