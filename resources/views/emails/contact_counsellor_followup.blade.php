@extends('layouts.email')

@section('email_content')
<h2>You were recently contacted by a potential client via CounsellorsUK</h2>

<p>If you have got back to them already, brilliant! If you have not, please do so
- they are waiting for you to get in touch.</p>

<p>The details they provided have been included below. 

@include('emails._contact_fields')
@endsection
