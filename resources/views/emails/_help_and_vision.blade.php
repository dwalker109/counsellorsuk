<p><strong>Here to help</strong></p>

<p>If you need any help, get in touch: 
    <a href="{{ route('contact') }}">{{ route('contact') }}</a>
</p>

<p><strong>CounsellorsUK’s Vision and Ethos</strong></p>

<p>The vision CounsellorsUK have for the future is that all the UK Counsellors and
Psychotherapists from the main professional bodies are listed on one, easy to
use site which focuses primarily on giving easy access to UK practitioners who
are qualified, credible and professional. </p>

<p>Individual members of the public, companies sourcing support on behalf of
employees, charities or medical staff will be able to look for a counsellor
anywhere within the UK and be assured of membership to a reputable professional
body with a category which endorses a high standard of achievement in the
profession and a minimum training standard*.</p>

<p><strong>*</strong> This includes the majority of which is taught face to 
face in a classroom, a supervised placement, regular supervision as per 
professional bodies code of ethics, personal therapy (usually of at least 
30 sessions) and continuing professional development (CPD) of usually over 
30 hours per year.</p>

<p>Our ethos at CounsellorsUK is about people and good mental health.  As the
professional bodies have been doing for years, we want to help work on the
arduous task of ending stigma of admitting to mental health issues and for
society to realise that this can affect anyone regardless of race, religion,
sexuality or postcode.  We would also like to see a move towards a more holistic
view of counselling, where people visit a ‘therapist’ to ensure they stay
mentally healthy, not just when they have an issue.</p>

<p>As part of our commitment to mental health, we are also supporting the charity -
<a href="http://www.youngminds.org.uk/">Young Minds</a>.</p>

<p>Please pass on information about CounsellorsUK to your colleagues and co-workers
to help ensure the public only receive emotional support from reputable and well
qualified counsellors and psychotherapists from the main UK professional bodies.</p>

