@extends('layouts.email')

@section('email_content')
<p>Please excuse the interruption, but we wanted to let you know that as a
qualified UK counsellor or psychotherapist, you have been selected to have a
free counsellor profile created on CounsellorsUK.</p>

<p><a href="{{ url('/') }}">{{ url('/') }}</a></p>

<p>We know how busy you are so to save your time, your details have been added
(you can update them immediately) because you belong to one of the main
UK professional bodies that we recognise as having: a code of ethics,
a complaints procedure and a minimum training level requirement <strong>*</strong>.</p>

<p><strong>Your member profile</strong></p>

<p>You can access and manage your profile now by using the below link to set up a password:</p>
<p><a href="{{ url('/password/reset') }}">{{ url('/password/reset') }}</a></p>

<p>Simply follow these steps:</p>

<ol>
 <li>Enter your email address ({{ $user->email }}) in the box</li>
 <li>Click the <strong>Send password reset link</strong> button</li>
 <li>You will be sent an email within a minute or two</li>
 <li>Open your email and click the link</li>
 <li>Enter your own password</li>
</ol>

@include('emails._help_and_vision')

<p><strong>What we want for our members</strong></p>

<p>There is no doubt about it, counselling and psychotherapy training is
expensive and with ongoing costs such as supervision, CPD and advertising, we
feel it’s important to keep costs to a minimum and support the counsellors and
psychotherapists within the profession.</p>

<p><strong>Removing your profile</strong></p>

<p>Your profile will not include any information that has not previously been
publicly available. If you would like us to remove your profile from
CounsellorsUK, please contact us using the below link and we shall remove your
details immediately.</p>
@endsection
