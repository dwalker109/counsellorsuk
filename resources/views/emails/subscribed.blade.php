@extends('layouts.email')

<?php $user = (object) $user // Queue converts passed data to array, so convert ?>

@section('email_content')
<p><strong>{{ $user->name }}, welcome to CounsellorsUK.  You now have a premium
profile and can now log into your CounsellorsUK member account:</strong></p>

<p><a href="{{ route('profile.view') }}">{{ route('profile.view') }}</a></p>

<p>Please use the email address ({{ $user->email }}) and password that you set during the registration
process. Don’t worry if you have forgotten your password, just use the forgotten
password facility within the login page.</p>

<p>The member account area will allow you to manage your entire profile and
keep your details up-to-date.</p>

@include('emails._help_and_vision')
@endsection
