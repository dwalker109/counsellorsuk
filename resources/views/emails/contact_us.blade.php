@extends('layouts.email')

@section('email_content')
<h2>A message has been sent via the CounsellorsUK contact form.</h2>

<p>The details provided have been included below. Please get back to them promptly.</p>

@include('emails._contact_fields')
@endsection