@extends('layouts.email')

@section('email_content')
<h2>Please verify your email address for CounsellorsUK</h2>

<p>Click the link below to verify ownership of this email address. Once this has
been completed, members of the public will be able to email you directly from 
<a href="{{ url('/') }}">CounsellorsUK</a>.</p>

<p><a href="{{ route('verify.process', $user->email_verify_token) }}">{{ route('verify.process', $user->email_verify_token) }}</a></p>

<p>If you cannot click the link, your email application is probably blocking it. 
Just copy and paste the link into your internet browser instead.</p>

<p><strong>Please note:</strong> CounsellorsUK will never reveal your email address
to the people who contact you. They will only see your email address once you
email them back.</p>
@endsection