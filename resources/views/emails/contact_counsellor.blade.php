@extends('layouts.email')

@section('email_content')
<h2>You have been contacted by a potential client via CounsellorsUK</h2>

<p>The details they provided have been included below.</p>

@include('emails._contact_fields')
@endsection
