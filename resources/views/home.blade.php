@extends('layouts.app')

@section('special_body_class', 'homepage')

@section('title', "Helping you find a counsellor from our $member_count counsellors across the UK")

@section('content')

<section class="homepage__section">
    <div class="homepage__main-cta">
        <div>
            <div class="homepage__blurb">
                <h2>Find a counsellor near you</h2>
                <h2>We give you direct access to over <strong>{{ $member_count }}</strong>
                Counsellors and Psychotherapists across the UK</h2>
            </div>
            <div class="homepage__searchbox">
                <p>Search for your location below, or see all available locations in our 
                <a href="{{ route('search.index') }}">counsellor index</a>.</p>
                {!! Former::inline_open()->route('search.prepare')->method('POST') !!}
                    {!! Former::select('location')
                        ->placeholder("{$click_verb} here and start typing your location")
                        ->options($location)
                        ->addClass('js-select2-ajax-lookup')
                        ->addClass('js-select2-submit-on-select')
                        ->setAttribute('data-ajax--url', '/api/lookup-location') !!}
                    {!! Former::primary_submit('Search')->addClass('js-select2-submit-on-select') !!}
                {!! Former::close() !!}
            </div>
            <div class="homepage__blurb">
                <h3>Counselling and therapy directory</h3>
                <h3>A <a href="{{ route('cms_page', $permalinks->get('cic')) }}">community interest company</a> 
                    re-investing profit into mental health</h3>
                <h3>Over {{ $member_count }} qualified counsellors, psychotherapists and CBT therapists</h3>
                <h3>All our counsellors are registered or accredited with the 
                    <a href="{{ route('cms_page', $permalinks->get('bodies')) }}">UK's main counselling and 
                    psychotherapy professional bodies</a></h3>
            </div>
        </div>
    </div>
</section>

<section class="homepage__section">
    <div class="homepage__portal">
        <div class="homepage__portal--card" data-mh="portals">
            <section>
                <div class="homepage__portal--cta">
                    <h3>Are you a counsellor or psychotherapist?</h3>
                    <a href="{{ url('/register') }}">Join</a>
                    <a href="{{ url('/login') }}">Login</a>
                </div>
                <div class="homepage__portal--image">
                    <img src="/build/img/are-you-a-therapist.jpg" alt="Are you a counsellor of psychotherapist?">
                </div>
            </section>
        </div>
    </div>
    
    <div class="homepage__portal">
        <div class="homepage__portal--card" data-mh="portals">
            <section>
                <div class="homepage__portal--cta">
                    <h3>In crisis? Seek help or advice.</h3>
                    <a href="{{ route('cms_page', 'in-crisis') }}">Talk</a>
                </div>
                <div class="homepage__portal--image">
                    <img src="/build/img/are-you-in-crisis.jpg" alt="Are you in crisis?">
                </div>
            </section>
        </div>
    </div>
</section>

<section class="homepage__section">
    <div class="homepage__portal--charity">
        <h3>We are proud to support</h3>
        <a href="http://www.youngminds.org.uk" target="_blank"><img src="/build/img/youngminds.svg" alt="YoungMinds"></a>
    </div>
</section>

@endsection
