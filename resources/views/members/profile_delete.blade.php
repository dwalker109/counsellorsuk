@extends('members._container')

@section('title', 'Delete')

@section('body')

{!! Former::open()->action('delete')->route('profile.do_delete')->addClass('profile__form') !!}
{{ csrf_field() }}

<fieldset class="profile__fieldset">
	<div class="profile__fieldset-label">Delete your profile</div>
	{!! Former::radios('confirmation')->label('Confirm delete?')->radios(['No', 'Yes'])->inline()
		->inlineHelp('Are you sure you want to permanently delete your profile?') !!}
	{!! Former::text('type_ok_to_continue')->label('Type OK to continue')
		->inlineHelp('Once deleted, your profile cannot be recovered. Please enter the word "OK" in the box to indicate that you understand this.') !!}		
</fieldset>

<div class="profile__action-buttons">
	<button type="submit" class="profile__action-buttons-delete">
		<span class="fa fa-trash"></span>
		Delete
	</button>
	<a class="profile__action-buttons-cancel" href="{{ route('profile.view') }}">
		<span class="fa fa-exclamation-circle"></span>
		Cancel
	</a>
</div>
{!! Former::close() !!}

@endsection
