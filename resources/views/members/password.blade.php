@extends('members._container')

@section('title', 'Change Password')

@section('body')
	
{!! Former::open() !!}
{{ csrf_field() }}

<fieldset class="profile__fieldset">

    {!! Former::password('current_password')->label('Current password') !!}
	{!! Former::password('new_password')->label('New password') !!}
    {!! Former::password('new_password_confirmation')->label('New password confirmation') !!}

</fieldset>

@include('members._profile_action_buttons')

{!! Former::close() !!}

@endsection
