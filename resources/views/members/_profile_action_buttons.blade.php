<div class="profile__action-buttons">
	<button type="submit" class="profile__action-buttons-save">
		<span class="fa fa-check-circle"></span>
		Save and return
	</button>
	<a class="profile__action-buttons-cancel" href="{{ route('profile.view') }}">
		<span class="fa fa-exclamation-circle"></span>
		Cancel without saving
	</a>
</div>