@extends('members._container')

@section('title', 'Practice Addresses')

@section('body')

{!! Former::open()->route('profile.update_addresses') !!}
{!! Former::populate($profile) !!}
{{ csrf_field() }}

<fieldset class="profile__fieldset">

	@foreach (['primary', 'additional'] as $index => $description)
		@if ($description === 'primary')
			<div class="profile__fieldset-label">Primary</div>
			{!! Former::hidden()->name("addresses[{$index}][add]")->forceValue(true) !!}
		@else
			<div class="profile__fieldset-label">Additional</div>
			{!! Former::radios()->label('Add an additional address?')->radios([
					'No' => [
						'name' => "addresses[{$index}][add]",
						'value' => '0', 
						'checked' => request()->old('addresses.{$index}.add]', !isset($profile->addresses[$index])),
					], 
					'Yes' => [
						'name' => "addresses[{$index}][add]", 
						'value' => '1', 
						'checked' => request()->old('addresses.{$index}.add]', isset($profile->addresses[$index])),
					],
				])->inline()->addClass('js-additional-address')->setAttribute('data-address-index', $index) !!}

		@endif

		<div id="address-{{ $index }}">
			{!! Former::text("addresses.{$index}.name")->name("addresses[{$index}][name]")->label('Practice name (optional)') !!}
			{!! Former::text("addresses.{$index}.address_1")->name("addresses[{$index}][address_1]")
				->label('Address line 1') !!}
			{!! Former::text("addresses.{$index}.address_2")->name("addresses[{$index}][address_2]")
				->label('Address line 2 (optional)') !!}
			{!! Former::select("addresses.{$index}.location_id")->name("addresses[{$index}][location_id]")
				->options($locations)->label('Town or London Borough')->placeholder("{$click_verb} here and start typing your location") 
				->addClass('js-select2-ajax-lookup-for-profile')
				->setAttribute('data-ajax--url', '/api/lookup-town') !!}
			{!! Former::text("addresses.{$index}.postal_code")->name("addresses[{$index}][postal_code]")->label('Postal code') !!}
			{!! Former::radios("addresses.{$index}.show_full_address")
				->name("addresses[{$index}][show_full_address]")
				->label('Display full address?')->inline()
				->radios(['No, only display the town', 'Yes']) !!}
		</div>
	@endforeach

</fieldset>

@include('members._profile_action_buttons')

{!! Former::close() !!}

@endsection
