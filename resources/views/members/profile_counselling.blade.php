@extends('members._container')

@section('title', 'Counselling')

@section('body')

{!! Former::open_for_files()->route('profile.update_counselling') !!}
{!! Former::populate($profile) !!}
{{ csrf_field() }}

<fieldset class="profile__fieldset">
	{!! Former::select('approaches')->options($approaches)->name('approaches[]') ->label('Approaches')->multiple() !!}
	{!! Former::select('issues')->options($issues)->name('issues[]')->label('Issues I work with') ->multiple() !!}
	{!! Former::select('skills')->options($skills)->name('skills[]')->label('Skills') ->multiple() !!}
	{!! Former::select('other_services')->options($other_services)->name('other_services[]') ->label('Other services I offer')->multiple() !!}
</fieldset>

@include('members._profile_action_buttons')

{!! Former::close() !!}

@endsection
