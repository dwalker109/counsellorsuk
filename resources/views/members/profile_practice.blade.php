@extends('members._container')

@section('title', 'Practice')

@section('body')

{!! Former::open()->route('profile.update_practice') !!}
{!! Former::populate($profile) !!}
{{ csrf_field() }}

<fieldset class="profile__fieldset">
	<div class="profile__fieldset-label">Sessions</div>
	{!! Former::text('session_lengths[initial]', 'Initial session')->append('minutes') !!}
	{!! Former::text('session_lengths[subsequent]', 'Subsequent sessions')->append('minutes') !!}
	{!! Former::checkboxes('appointments_at[]')->label('Appointments offered at')
        ->checkboxes([
            'Weekdays' => ['name' => 'appointments_at[weekdays]', 'value' => '1'],
            'Some Evenings' => ['name' => 'appointments_at[some_evenings]', 'value' => '1'],
            'Weekends' => ['name' => 'appointments_at[weekends]', 'value' => '1'],
        ])->inline() !!}
</fieldset>

<fieldset class="profile__fieldset">
	<div class="profile__fieldset-label">Fees</div>
	{!! Former::text('fees[adults]', 'Adults')->prepend('&pound;') !!}
	{!! Former::text('fees[adolescents]', 'Adolescents')->prepend('&pound;') !!}
	{!! Former::text('fees[families]', 'Families')->prepend('&pound;') !!}
	{!! Former::text('fees[couples]', 'Couples')->prepend('&pound;') !!}
	{!! Former::text('fees[children]', 'Children')->prepend('&pound;') !!}
	{!! Former::text('fees[concessions]', 'Concessions')->prepend('&pound;') !!}
</fieldset>

<fieldset class="profile__fieldset">
	<div class="profile__fieldset-label">Concessions</div>
	{!! Former::radios('probono_available')->label('Do you offer any concessionary sessions?')
	        ->radios(['No', 'Yes'])->inline() !!}
	{!! Former::text('probono_about', 'Information on concessionary sessions, if offered') !!}
</fieldset>

@include('members._profile_action_buttons')

@endsection
