@extends('members._container')

@section('title', $page->title)
@section('meta_description', $page->meta_description)

@section('body')

<section>
	<div class="profile__member-benefits">
		{!! $page->body !!}
	</div>
</section>

<div class="profile__member-benefits__links">
	@foreach($children as $child)
		<a class="js-cms-modal" href="{{ route('cms_page', $child) }}">{{ $child->title }}</a>
	@endforeach
</div>

@include('cms_page._bootstrap_modal')

@endsection
