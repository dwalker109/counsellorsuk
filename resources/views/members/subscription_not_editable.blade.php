@extends('members._container')

@section('title', 'Subscription cannot be edited online')

@section('body')

<fieldset class="profile__fieldset">
    <div class="profile__fieldset-label">Sorry, your subscription cannot be edited online</div>

    <p>Your subscription was created on our original site and cannot be edited online.</p>
    <p>Please <a href="{{ route('contact') }}">contact us</a> to amend your subscription.</p>
</fieldset>    

@endsection