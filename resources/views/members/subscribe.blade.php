@extends('members._container')

@section('title', 'Subscribe to CounsellorsUK')

@section('body')

<section>

	<div class="subscription__intro">
		<h2>CounsellorsUK subscription</h2>
		<p>With a full membership of CounsellorsUK, you can enjoy all of these benefits:</p>
		<ul class="subscription__bullets">
			<li><span class="fa fa-check"></span>Priority placement in search results</li>
			<li><span class="fa fa-check"></span>Display of your full CounsellorsUK profile to prospective patients</li>
			<li><span class="fa fa-check"></span>Access to Public Indemnity Insurance at a special members rate</li>
		</ul>
	</div>

	<div class="subscription__payform">

		<h3>{!! Converter::to('currency.gbp')->value($plan->price)->format() !!} {{ $plan->name }}</h3>
		<p>Your subscription will automatically renew each year. You can pay with PayPal, or a credit/debit
		card - just complete the form below for instant access.</p>

		<form action="{{ route('subscription.new') }}" method="POST">
			{{ csrf_field() }}
			<input type="hidden" name="plan" id="braintree_plan" value="{{ $plan->id }}">
			<div id="braintree-container"></div>

			<div class="subscription__coupon">
				<label for="coupon">Do you have a CounsellorsUK voucher? If so, enter it here:</label>
				<input name="coupon">
			</div>

			<input class="subscription__submit" type="submit" value="Start subscription">
		</form>

	</div>

</section>

@endsection