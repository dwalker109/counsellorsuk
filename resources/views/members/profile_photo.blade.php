@extends('members._container')

@section('title', 'Contact Information')

@section('body')

{!! Former::open_for_files()->route('profile.delete_photo')->method('DELETE') !!}
{{ csrf_field() }}

	<div class="profile__photoedit-show">
		@if ($profile->has_photo)
			<button type="submit" class="profile__photoedit-remove-button">
				<span class="fa fa-trash"></span>
				Remove
			</button>
		@endif
		<img src="{{ $profile->photos->large }}">
	</div>

{!! Former::close() !!}

{!! Former::open_for_files()->route('profile.update_photo')->addClass('profile__form') !!}
{{ csrf_field() }}

	<div class="profile__photoedit-form">

		<input type="file" name="photo" class="filestyle" data-buttonText="Upload a new profile photo"
		data-buttonName="btn-default" data-iconName="fa fa-photo">

		@if ($errors->has('photo'))
			<div class="profile__photoedit_errors">
		        @foreach($errors->get('photo') as $message)
		            <p>{{ $message }}</p>
		        @endforeach
			</div>
		@endif

	</div>

	@include('members._profile_action_buttons')

{!! Former::close() !!}

@endsection
