<p>Something went wrong while attempting to use your payment method.</p>

<ul>
    <li>Ensure you have entered your card or PayPal details correctly</li>
    <li>Check with your issuer for a problem with your payment method</li>
    <li>Try a different payment method</li>
</ul>

<p>If you continue to experience problems, please <a href="{{ route('contact') }}">contact us</a> for assistance.</p>