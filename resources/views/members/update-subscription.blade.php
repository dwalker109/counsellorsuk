@extends('members._container')

@section('title', 'Update your payment details')

@section('body')

<section>

	@if ($user->subscription($subscription_name)->onGracePeriod())
		{{-- On grace period, allow resuming --}}
		<div class="subscription__intro">
			<h2>Resume your subscription</h2>
			<p class="plan-chooser__already-subscribed">Your subscription has previously been cancelled.
			You can resume it instantly with the button below. You will not be charged again until your
			renewal date.</p>
		</div>

		<div class="subscription__payform">
			<form action="{{ route('subscription.resume') }}" method="POST">
				{{ csrf_field() }}
				<input class="subscription__submit" type="submit" value="Resume subscription">
			</form>
		</div>
	@else
		<div class="subscription__intro">
			<h2>Update your payment method</h2>
			<p>You are already subscribed to CounsellorsUK, but you can change your
			payment method below. You will not be charged again until your renewal date.</p>
		</div>

		<div class="subscription__payform">
			<form action="{{ route('subscription.update_card') }}" method="POST">
				{{ csrf_field() }}
				<div id="braintree-container"></div>
				<input class="subscription__submit" type="submit" value="Save">
			</form>
		</div>
	@endif

</section>

@endsection