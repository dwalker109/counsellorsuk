@extends('members._container')

@section('title', 'Details')

@section('body')

{!! Former::open()->route('profile.update_details')->addClass('profile__form') !!}
{!! Former::populate($profile) !!}
{{ csrf_field() }}

<fieldset class="profile__fieldset">
	<div class="profile__fieldset-label">Professional</div>
	{!! Former::text('registration_number') !!}
	{!! Former::text('bacp_certificate_number')->label('BACP certificate number') !!}
	{!! Former::text('main_qualification') !!}
	{!! Former::select('body_memberships')->options($body_memberships)->name('body_memberships[]')->label('Professional Body memberships')->multiple() !!}
	{!! Former::radios('is_insured')->label('Are you insured?')->radios(['No', 'Yes'])->inline()
		->inlineHelp('By selecting yes are confirming that you have a valid insurance policy in place.') !!}
</fieldset>

<fieldset class="profile__fieldset">
	<div class="profile__fieldset-label">About Me</div>
	<p>Use this section to tell people all about yourself. We recommend you include
	background on why you became a counsellor, as well as further information on
	your qualifications, training and experience.</p>
	<div class="form-group {{ $errors->has('about') ? 'has-error' : null }}">
		<div class="col-xs-12">
			<textarea class="form-control" id="about" name="about">{!! Purifier::clean(request()->old('about', $profile->about)) !!}</textarea>
			@if ($errors->has('about'))
				<span class="help-block">{{ $errors->first('about') }}</span>
			@endif
		</div>
	</div>
</fieldset>

<fieldset class="profile__fieldset">
	<div class="profile__fieldset-label">Counselling experience</div>
	{!! Former::text('counselling_hours[total]', 'Total')->append('hours') !!}
	{!! Former::text('counselling_hours[years]', 'across')->append('years') !!}
</fieldset>


@include('members._profile_action_buttons')

{!! Former::close() !!}

@endsection
