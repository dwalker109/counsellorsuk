@extends('members._container')

@section('title', 'Subscription cannot be edited online')

@section('body')

<fieldset class="profile__fieldset">
    <div class="profile__fieldset-label">Sorry, your profile cannot be deleted at the moment</div>

    <p>You have an active subscription to the site, which needs to be cancelled before your profile is deleted.</p>
    <p>Please <a href="{{ route('contact') }}">contact us</a> to cancel your subscription.</p>
</fieldset>    

@endsection