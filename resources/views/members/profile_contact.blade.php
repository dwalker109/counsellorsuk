@extends('members._container')

@section('title', 'Details')

@section('body')

{!! Former::open()->route('profile.update_contact')->addClass('profile__form') !!}
{!! Former::populate($profile) !!}
{{ csrf_field() }}

<fieldset class="profile__fieldset">
	<div class="profile__fieldset-label">Name and email</div>
	{!! Former::text('name')->value($profile->user->name)->label('Name')
		->inlineHelp('Will be used in search results, and also appears at the top of your profile.') !!}
	{!! Former::text('email')->value($profile->user->email)->label('Email')
		->inlineHelp('The address you use to log in, and to receive any referrals sent via the site.') !!}
</fieldset>

<fieldset class="profile__fieldset">
	<div class="profile__fieldset-label">Telephone numbers</div>
	{!! Former::text('phone_numbers[primary]')->label('Primary') !!}
	{!! Former::text('phone_numbers[additional]')->label('Additional') !!}
</fieldset>

<fieldset class="profile__fieldset">
	<div class="profile__fieldset-label">Website addresses</div>
	{!! Former::text('urls[primary]')->label('Primary')->inlineHelp($url_help = 'Please copy and paste your full address, including the <b>http://</b> or <b>https://</b> at the start.') !!}
	{!! Former::text('urls[additional]')->label('Additional')->inlineHelp($url_help) !!}
</fieldset>

<fieldset class="profile__fieldset">
	<div class="profile__fieldset-label">Social media</div>
	{!! Former::text('social_media[facebook]')->label('Facebook')
		->inlineHelp('Go to Facebook. Click your profile icon and then copy the URL. Example: <b>https://www.facebook.com/counsellorsukorg</b>') !!}
	{!! Former::text('social_media[twitter]')->label('Twitter')
		->inlineHelp('Go to Twitter. Click the <b>Me</b> tab and then copy the the URL. Example: <b>https://twitter.com/counsellors_UK</b>') !!}
	{!! Former::text('social_media[linkedin]')->label('Linkedin')
		->inlineHelp('Go to LinkedIn. Copy the URL visible in the <b>Your public profile URL</b> section. Example: <b>http://www.linkedin.com/company/counsellorsuk-org</b>') !!}
</fieldset>

@include('members._profile_action_buttons')

{!! Former::close() !!}

@endsection
