@extends('members._container')

@section('title', 'My Profile')

@section('body')

<section>
    <div class="profile__completion">
        <h2>Profile completion</h2>
        
        <div class="progress">
            <div class="progress-bar progress-bar-{{ $profile->completion_percentage < 100 ? 'warning' : 'success' }}" 
                role="progressbar"aria-valuenow="{{ $profile->completion_percentage }}" aria-valuemin="0" aria-valuemax="100"
                style="width: {{ $profile->completion_percentage }}%">
                    <span class="sr-only">{{ $profile->completion_percentage }}% complete</span>
            </div>
        </div>

        @if ($profile->completion_percentage < 100)
            <p>Completing 100% of your profile will help to maximise the quality of referrals you receive.</p>
            <h3>What should I do next?</h3>
            <p>Complete your <a href="{{ route($profile->stages->next->route) }}">{{ lcfirst($profile->stages->next->name) }}.</a></p>
        @else 
            <p><a href="{{ route('counsellor', [Auth::user()->profile->slug]) }}">Your profile</a> 
            is 100% complete! You should ensure you keep it up to date should anything change. 
            Just visit this section at any time to make amendments.</p>
        @endif

    </div>
</section>

<section>

    @foreach ($profile->stages->all as $stage)
    	<div class="profile__stage">

            <div class="profile__card" data-mh="profile-card">

                <span class="profile__stage-complete-{{ $stage->complete ? 'true' : 'false' }}">
                    {{ $stage->complete ? 'Complete' : 'Incomplete' }}
                </span>

    			<h2>{{ $stage->name }} </h2>
    			<p>{{ $stage->description }}</p>
    			<a class="profile__update-stage" href="{{ route($stage->route) }}">
                    <span class="fa {{ $stage->fa_icon }}"></span>
                    Update
                </a>
            </div>

    	</div>
    @endforeach

</section>

<section>

    <div class="profile__delete">
        <p>No longer want your profile? We're sorry to see you go, but we've made it simple 
        to permanently <a href="{{ route('profile.delete') }}">delete your account</a>.</p>
    </div>

</section>

@endsection
