@extends('layouts.app')

@section('content')

    @if (Request::is('profile*'))
        @if (!Auth::user()->profile->meta_is_subscriber && !Session::has('just_subscribed'))
            <div class="profile__nag-wrapper">
                You are currently using a free profile. This means that:
                <ul>
                    <li>Only a limited version of your profile will be visible to the public
                        <span class="fa fa-question-circle" data-toggle="tooltip"
                        title="Only the first {{ config('counsellorsuk.free_profile_about_limit') }}
                        characters of your biography will appear, and fees, session lengths,
                        appointment options and concessionary information will not be shown."></span></li>
                    <li>You will appear lower in search results than any paid profiles</li>
                </ul>
                To reach as many potential clients as possible,
                <a href="{{ route('subscription.view') }}">subscribe to CounsellorsUK!</a>
            </div>
        @endif

        @if (!Auth::user()->email_verified && Auth::user()->email_verify_token)
            <div class="profile__nag-wrapper">
                Your email address is pending verification. Please check your email and
                follow the instruction we have sent to you.
                You can <a href="{{ route('verify.send') }}">re-send the email</a>
                if you have deleted it.
            </div>
        @elseif (!Auth::user()->email_verified)
            <div class="profile__nag-wrapper">
                You should <a href="{{ route('verify.send') }}">verify your email address</a>
                to ensure people can send you messages directly from your profile.
            </div>
        @endif
    @endif

    <ul class="member__nav">
        <li class="{{ Request::is('profile*') ? 'active' : null }}">
            <a href="{{ route('profile.view') }}">Edit Profile</a>
        </li>
        <li class="{{ Request::is('subscribe') ? 'active' : null }}">
            <a href="{{ route('subscription.view') }}">Subscription</a>
        </li>
        <li class="{{ Request::is('change-password') ? 'active' : null }}">
            <a href="{{ route('password.view') }}">Change Password</a>
        </li>
    </ul>

    @unless ($errors->isEmpty())
        <div class="profile__errors">
            <p>Some issues were found when trying to save. Please resolve them, then try again.</p>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endunless

    @yield('body')

@endsection
