<p>You are now subscribed to CounsellorsUK!</p>

<ul>
    <li>We recommend you take the time to complete every section of your profile, via the options below</li>
    <li>Please do not hesitate to <a href="{{ route('contact') }}">contact us</a> if you need help</li>
</ul>