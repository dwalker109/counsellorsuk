@unless ($profile->body_memberships->isEmpty())
    <h3>Professional memberships</h3>
    <p>{{ $profile->present()->body_memberships_full }}</p>
@endunless