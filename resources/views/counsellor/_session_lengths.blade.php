@unless ($profile->present()->session_lengths->isEmpty())
    <h3>Session lengths</h3>
    <ul>
        @foreach ($profile->present()->session_lengths as $session)
            <li>{{ $session->type }}: {!! $session->length !!} minutes</li>
        @endforeach
    </ul>
@endunless
