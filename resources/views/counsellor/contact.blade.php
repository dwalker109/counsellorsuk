@extends('layouts.app')

@include('meta/_contact')

@section('content')

<section>
    
    <div class="counsellor__contact-form">

        <h2 class="counsellor__contact-heading">Contact {{ $profile->user->name }} by email</h2>
        
        <p>Complete this form to send an email message directly to {{ $profile->user->name }}.
        Please ensure your contact details are correct so they can get back to you promptly.</p>
        
        {!! Former::open() !!}
        
            {!! Former::text('name')->label('Your name') !!}
            {!! Former::text('email')->label('Your email address') !!}
            {!! Former::text('phone')->label('Your phone number') !!}
            {!! Former::inline_radios('contact_preference')->radios([
                    'Email' => ['value' => 'email'], 
                    'Phone' => ['value' => 'phone'], 
                    'Other (details in message)' => ['value' => 'other'],
                ]) !!}
            {!! Former::textarea('message')->label('Your message')->class('no-wysiwyg')
                ->placeholder('Hi! I found your details on CounsellorsUK and I would like to talk.') !!}
                
            <div class="form-group {{ $errors->has('g-recaptcha-response') ? 'has-error' : null }}">
                <div class="former-recaptcha">    
                    {!! Recaptcha::render() !!}
                    @if ($errors->has('g-recaptcha-response'))
                        <span class="help-block">{{ $errors->first('g-recaptcha-response') }}</span>
                    @endif
                </div>
            </div>
                
            <div class="counsellor__contact-buttons">
                <button type="submit">Contact {{ $profile->user->name }}</button>
                <a href="{{ route('counsellor', $profile) }}">View full profile</a>
            </div>
        
        {!! Former::close() !!} 

    </div>

    <div class="counsellor__contact-miniprofile">
        @include('counsellor._photo_card')
    </div>    

</section>
@endsection
