@include('_statistics_record', ['tag' => Statistics::PROFILE_VIEWED])

@extends('layouts.app')

@include('meta/_profile')

@section('content')

<section>
    <div class="counsellor__basic">
        @include('counsellor._photo_card')
        @include('counsellor._body_memberships')
        @include('counsellor._counselling_hours')
        @include('counsellor._main_qualification')
        @include('counsellor._fees')
        @include('counsellor._session_lengths')
        @include('counsellor._appointments')
        @include('counsellor._concessions')
    </div>

    <div class="counsellor__advanced">
        @include('counsellor._about')
        @include('counsellor._addresses')
        @include('counsellor._support_items')
    </div>
</section>
@endsection
