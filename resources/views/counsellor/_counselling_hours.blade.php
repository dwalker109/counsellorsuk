@unless (!$profile->present()->counselling_hours)
    <h3>Counselling hours</h3>
    <p>{{ $profile->present()->counselling_hours }}</p>
@endunless
