<div class="counsellor__photo">
    <img src="{{ $profile->photos->large }}"
        alt="Counselling in {{ $profile->present()->locations }}, {{ $profile->user->name }}">
    @if ($profile->is_verified)
        <img src="/build/img/verified.png"
        alt="{{ $profile->user->name }} is a verified member of CounsellorsUK"
        title="{{ $profile->user->name }} is a verified member of CounsellorsUK"
        data-toggle="tooltip" data-placement="right" data-container="body">
    @endif
    <div>
        <h3>{{ $profile->user->name }}</h3>
        <ul>
            @foreach ($profile->present()->phone_numbers as $phone_number)
                <li><span class="fa fa-phone"></span>
                {{ $phone_number }}</li>
            @endforeach
            @if (config('counsellorsuk.allow_contact_to_unverified_email') || $profile->user->email_verified)
                <li><span class="fa fa-envelope"></span>
                     <a href="{{ route('counsellor.contact', $profile) }}">Contact via email</a></li>
            @endif
            @foreach ($profile->present()->urls as $url)
                <li><span class="fa fa-share-alt"></span>
                <a href="{{ $url->href }}" target="_blank">{{ $url->text }}</a></li>
            @endforeach
        </ul>
        @if (!Request::is('counsellors/*/claim') && Auth::guest() && !$profile->user->last_login_at)
            <ul>
                <li><span class="fa fa-user-plus"></span> Is this you?
                <a href="{{ route('profile.claim', $profile) }}">Claim your profile</a></li>
            </ul>
        @endif
        <ul>
            @if (session()->has('last_search_url'))
                <li><a href="{{ session()->get('last_search_url') }}">&lArr; Back to search results</a></li>
            @endif
            <li><a href="{{ route('search.index') }}">Start a new search &rArr;</a></li>
        </ul>
    </div>
</div>
