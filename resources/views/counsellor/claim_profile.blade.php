@extends('layouts.app')

@section('title')
Claim profile for {{ $profile->user->name }} in {{ $profile->present()->locations }}
@endsection

@section('content')

<section>
    
    <div class="counsellor__claim-form">

        <h2 class="counsellor__claim-heading">Claim this profile</h2>
        
        <p>If you are {{ $profile->user->name }} you can claim this profile. This will allow you to update it,
            as well as upgrade to a professional profile for a small annual fee.</p>

        <p>We will send you an email to verify your identity and allow you to log in straight away. If you don't 
            get an email within a few minutes, we might not have your current address. If you need assistance with 
            this or have any other questions, please <a href="{{ route('contact') }}">contact us</a>.
        
        {!! Former::open() !!}
        
            <div class="form-group {{ $errors->has('g-recaptcha-response') ? 'has-error' : null }}">
                <div class="counsellor__claim-recaptcha">
                        {!! Recaptcha::render() !!}
                        @if ($errors->has('g-recaptcha-response'))
                            <span class="help-block">{{ $errors->first('g-recaptcha-response') }}</span>
                        @endif
                </div>
            </div>
                
            <div class="counsellor__claim-buttons">
                <button type="submit">I am {{ $profile->user->name }}, claim this profile</button>
                <a href="{{ route('counsellor', $profile) }}">Do not claim this profile</a>
            </div>
        
        {!! Former::close() !!} 

    </div>

    <div class="counsellor__claim-miniprofile">
        @include('counsellor._photo_card')
    </div>    

</section>
@endsection
