<section>
    @unless ($profile->about === null)
        <div class="counsellor__about">
            <h2 class="counsellor__about-heading">Counsellor / Psychotherapist Profile</h2>
            <div>{!! $profile->present()->about !!}</div>
        </div>
    @endunless
</section>
