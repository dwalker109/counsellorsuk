@include('_statistics_record', ['tag' => Statistics::PROFILE_VIEWED])

@extends('layouts.app')

@include('meta/_profile')

@section('content')

<section>
    <div class="counsellor__basic">
        @include('counsellor._photo_card')
        @include('counsellor._body_memberships')
        @include('counsellor._counselling_hours')
        @include('counsellor._main_qualification')
        
        @if (!Auth::guest() && Auth::user()->id === $profile->id)
            <div class="counsellor__upgrade-nag">
                <p>Your free profile does not display your fees, session lengths,
                appointment options or concessionary information.</p>
                <p><a href="{{ route('subscription.view') }}">Subscribe now to 
                upgrade</a>
            </div>
        @endif
        
    </div>

    <div class="counsellor__advanced">
        @include('counsellor._about')

        @if (!Auth::guest() && Auth::user()->id === $profile->id)
            <div class="counsellor__upgrade-nag">
                <p>Your free profile only displays the first
                {{ config('counsellorsuk.free_profile_about_limit') }}
                characters of biography information.</p>
                <p><a href="{{ route('subscription.view') }}">Subscribe now to 
                upgrade</a>
            </div>
        @endif
        
        @include('counsellor._addresses')
        @include('counsellor._support_items')
    </div>
</section>
@endsection
