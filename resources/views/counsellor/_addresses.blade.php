<section>
    @foreach ($profile->addresses as $key => $address)
        <div class="counsellor__address-card">
            <div class="counsellor__map">
                @if ($address_located[$key])
                    {!! Mapper::render($key) !!}
                @else
                    <div class="counsellor__map--address-not-found">
                        <span class="fa fa-exclamation-circle"></span>
                        <p>Address could not be located on the map!</p>
                    </div>
                @endif
            </div>
            <div class="counsellor__street-address">
                {!! $address->present()->address_block !!}
            </div>
        </div>
    @endforeach
</section>
