@unless ($profile->present()->appointments_at->isEmpty())
    <h3>Appointments</h3>
    <ul>
        @foreach ($profile->present()->appointments_at as $appointment_at)
            <li>{{ $appointment_at }}</li>
        @endforeach
    </ul>
@endunless
