@unless (!$profile->probono_available)
    <h3>Available for concessionary rate counselling</h3>
    <p>{{ $profile->probono_about }}</p>
@endunless
