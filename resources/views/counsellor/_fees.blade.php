@unless ($profile->present()->fees->isEmpty())
    <h3>Fees</h3>
    <ul>
        @foreach ($profile->present()->fees as $fee)
            <li>
                {{ $fee->type }}: {!! $fee->price !!}
                @if ($fee->tooltip)
                    <span class="fa fa-question-circle" data-toggle="tooltip" 
                    title="{{ $fee->tooltip }}"></span>
                @endif
            </li>
        @endforeach
    </ul>
@endunless
