<section>
    <div class="counsellor__skills">
        @unless ($profile->skills->isEmpty())
            <div class="counsellor__card" data-mh="skills-services">
                <h3>Skills</h3>
                <ul>
                    @foreach ($profile->skills as $skill)
                        <li>
                            <img src="{{ $skill->icon }}" alt="{{ $skill->name }}">
                            {{ $skill->name }}
                        </li>
                    @endforeach
                </ul>
            </div>
        @endunless
    </div>

    <div class="counsellor__services">
        @unless ($profile->other_services->isEmpty())
            <div class="counsellor__card" data-mh="skills-services">
                <h3>Services</h3>
                <ul>
                    @foreach ($profile->other_services as $service)
                        <li>
                            <img src="{{ $service->icon }}" alt="{{ $service->name }}">
                            {{ $service->name }}
                        </li>
                    @endforeach
                </ul>
            </div>
        @endunless
    </div>

    <div class="counsellor__issues">
        @unless ($profile->issues->isEmpty())
            <div class="counsellor__card">
                <h3>Issues I work with</h3>
                <p>{{ $profile->present()->issues }}</p>
            </div>
        @endunless
    </div>
</section>
