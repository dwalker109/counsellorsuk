<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title') - Counselling directory for the UK - Counsellors UK</title>
    <meta name="description" content="@yield('meta_description', 'CounsellorsUK - helping you find a counsellor')">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Sarala:400,700|Raleway' rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="{{ elixir('css/app.css') }}" rel="stylesheet">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="57x57" href="/build/img/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/build/img/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/build/img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/build/img/favicons/apple-touch-icon-76x76.png">
    <link rel="icon" type="image/png" href="/build/img/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/build/img/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/build/img/favicons/manifest.json">
    <link rel="mask-icon" href="/build/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="/build/img/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="/build/img/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
</head>

<body id="app-layout" class="@yield('special_body_class', null)">
    <!-- Analytics -->
    {!! Analytics::render() !!}

    @if (Auth::user())
        <div class="container">
            <section class="profile__signed-in-banner">
                <div class="profile__signed-in"><p>Signed in as {{ Auth::user()->name }}</p></div>
                <div class="profile__signed-in">
                    <a class="profile__signed-in-cta" href="{{ route('dashboard') }}">Edit Profile</a>
                    @if (Auth::user()->profile->is_live)
                        <a class="profile__signed-in-cta" href="{{ route('counsellor', Auth::user()->profile) }}">View Profile</a>
                    @endif
                    <form class="profile__logout-form" action="{{ url('/logout') }}" method="POST">
                        {{ csrf_field() }}
                        <button type="submit" class="profile__signed-in-cta" href="{{ url('/logout') }}">Sign out</button>
                    </form>
                </div>
            </section>
        </div>
    @endif

    <nav class="header">
        <div>
            <div class="header__branding">
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Brand -->
                <span class="header__title"><a href="{{ route('home') }}">Counsellors UK</a></span>
            </div>
            <div class="header__nav" id="app-navbar-collapse">
                <ul class="header__nav-items">
                    <li><a href="{{ route('cms_page', 'in-crisis') }}">In crisis</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                        role="button" aria-haspopup="true" aria-expanded="false">Therapies <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            @foreach ($therapies as $page)
                                <li><a href="{{ route('cms_page', $page) }}">{{ $page->title }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                        role="button" aria-haspopup="true" aria-expanded="false">What concerns you <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            @foreach ($what_concerns_you as $page)
                                <li><a href="{{ route('cms_page', $page) }}">{{ $page->title }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                        role="button" aria-haspopup="true" aria-expanded="false">About <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            @foreach ($about as $page)
                                <li><a href="{{ route('cms_page', $page) }}">{{ $page->title }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                        role="button" aria-haspopup="true" aria-expanded="false">For Counsellors<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            @foreach ($counsellors_area as $page)
                                <li><a href="{{ route('cms_page', $page) }}">{{ $page->title }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    @if (Auth::guest())
                        <li><a href="/register">Join</a></li>
                        <li><a href="/login">Login</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    <!-- Notifications -->
    <div class="container">
        <section>
            <div class="notification-bar">
                @notification()
            </div>
        </section>
    </div>

    <!-- Main content -->
    <div class="container">
        @yield('content')
    </div>

    <!-- Fix spacing issues caused by JS height matched items -->
    <div class="match-height-clearfix">&nbsp;</div>

    <!-- Footer -->
    <footer class="footer">
        <section class="footer__section">
            <div class="footer__contact">
                <addr>
                    Registered Office:<br>
                    Menta Business Centre<br>
                    5 Eastern Way, Bury St Edmunds<br>
                    Suffolk IP32 7AB
                </addr>
            </div>
            <div class="footer__links">
                <a href="{{ route('search.index') }}">Counsellor index</a><br>
                <a href="{{ route('contact') }}">Contact</a><br>
                <a href="{{ route('cms_page', 'privacy-policy') }}">Privacy policy</a><br>
                <a href="{{ route('cms_page', 'terms-and-conditions') }}">Terms &amp; conditions</a>
            </div>
            <div class=footer__social-media>
                <a href="https://twitter.com/counsellors_UK" target="_blank"><span class="fa fa-twitter" aria-hidden="true"></span></a>
                <a href="https://www.facebook.com/counsellorsukcic" target="_blank"><span class="fa fa-facebook-official" aria-hidden="true"></span></a>
                <a href="https://www.instagram.com/counsellors_uk" target="_blank"><span class="fa fa-instagram" aria-hidden="true"></span></a>
            </div>
            <div class="footer__copyright">
                &copy; {{ \Carbon\Carbon::now()->format('Y') }} CounsellorsUK CIC, registered in England and Wales 10522539
            </div>
        </section>
    </footer>

    <!-- JavaScripts -->
    <script src="{{ elixir('js/dependencies.js') }}"></script>
    <script src="{{ elixir('js/app.js') }}"></script>

    <!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
    <script type="text/javascript">
        window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":null,"theme":"light-bottom"};
    </script>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
    <!-- End Cookie Consent plugin -->

</body>
</html>
