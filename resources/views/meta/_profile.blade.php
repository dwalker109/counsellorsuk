@section('title')
Counselling - {{ $profile->user->name }} in {{ $profile->present()->locations }}
@endsection

@section('meta_description')
Counselling directory - counselling / therapy in {{ $profile->present()->issues }}
@endsection
