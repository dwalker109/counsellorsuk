@section('title')
Counselling and therapy 

{{-- Include issue if one has been chosen --}}
@if ($issue_selected = ! empty(array_diff_assoc($issue, config('counsellorsuk.search.unfiltered.issue'))))
for {{ last($issue) }}
@endif

in {{ last($location) }}    
@endsection

@section('meta_description')
@if ($issue_selected)
Counselling directory for {{ last($issue) }} in {{ last($location) }}. Check for face to face, online and phone counselling and home visits.
@else
Qualified counselling for {{ last($location) }}. Search directory by anxiety, depression, mental health, bereavement, stress, self harm, addiction, domestic violence, anger management, eating disorders.
@endif
@endsection
