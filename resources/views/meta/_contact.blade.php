@section('title')
Contact counsellor - {{ $profile->user->name }} in {{ $profile->present()->locations }}
@endsection

@section('meta_description')
Counselling ({{ $profile->main_qualification }}) in {{ $profile->present()->locations }}. Directory of counsellors, therapists and psychotherapists in the UK.
@endsection
