@include('_statistics_record', ['tag' => Statistics::PROFILE_SEARCH_RESULTS])

<div class="search-results__card">
	<section>
		<div class="search-results__photo">
			<a href="{{ route('counsellor', $profile) }}">
				<img src="{{ $profile->photos->thumbnail }}"
					alt="Counselling in {{ $profile->present()->locations }}, {{ $profile->user->name }}">
			@if ($profile->is_verified)
				<img src="/build/img/verified.png"
				alt="{{ $profile->user->name }} is a verified member of CounsellorsUK"
                title="{{ $profile->user->name }} is a verified member of CounsellorsUK"
                data-toggle="tooltip" data-placement="right" data-container="body">
			@endif
			</a>
		</div>

		<div class="search-results__basic-info">
			<h3><a href="{{ route('counsellor', $profile) }}">{{ $profile->user->name }}</a></h3>

		    @unless ($profile->addresses->isEmpty())
		        <ul>
			        @foreach (collect($profile->addresses)->unique('location.name')->sortBy('location.distance') as $address)
			        	<li>
			                <a href="{{ route('search.results', $address->location->slug) }}">
			                	{{  $address->location->name }}
			                </a>
			                {{ $address->present()->location_distance }}
			        	</li>
			        @endforeach
		        </ul>
		    @endunless

			@unless ($profile->body_memberships->isEmpty())
				<p>{{ $profile->present()->body_memberships_full }}</p>
			@endunless
		</div>

		<div class="search-results__other-services">
			@unless ($profile->other_services->isEmpty())
				<ul>
					@foreach ($profile->other_services as $service)
						<li>
							<img alt="{{ $service->name }}" src="{{ $service->icon }}"
								class="search-results__service-icons">
							{{ $service->name }}
						</li>
					@endforeach
				</ul>
			@endunless
		</div>
	</section>

	<section class="search-results__card-lower">
		<div class="search-results__skills">
			@unless ($profile->skills->isEmpty())
				<ul>
					@foreach ($profile->skills as $skill)
						<li>
							<img alt="{{ $skill->name }}" src="{{ $skill->icon }}"
								class="search-results__service-icons">
							{{ $skill->name }}
						</li>
					@endforeach
				</ul>
			@endunless
		</div>

		<div class="search-results__cta-buttons">
			@unless ($profile->issues->isEmpty())
				<button class="btn btn-default btn-block" data-toggle="modal" 
				data-target="#modal-{{ $profile->id }}">Issues I work with</button>
			@endunless

			<a class="btn btn-default btn-block" href="{{ route('counsellor', $profile) }}">View full profile</a>

			<a class="btn btn-default btn-block" href="{{ route('counsellor.contact', $profile) }}">Contact me</a>
		</div>

		@unless ($profile->issues->isEmpty())
			<div id="modal-{{ $profile->id }}" class="modal fade" tabindex="-1" role="dialog">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<div class="modal-body">
							<p>{{ $profile->present()->issues }}</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		@endunless

	</section>
</div>