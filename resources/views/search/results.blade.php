@extends('layouts.app')

@include('meta._results')

@section('content')

<section class="search">
    <div class="search-choices">
        @include('search._form')
        @include('_signup')
    </div>

    <div class="search-results">
        @unless (($results = $results ?? null) === null)
            <h2>Counselling directory results in and near {{ last($location) }}</h2>
            {{ $results->links() }}
        @endunless

        @each('search._listing', $results, 'profile', 'search._no_items')

        @unless (($results = $results ?? null) === null)
            {{ $results->links() }}
        @endunless
    </div>
</section>
@endsection
