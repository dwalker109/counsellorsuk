@extends('layouts.app')

@section('title', 'Counsellor index')
@section('meta_description', "CounsellorsUK index of all UK counties and towns")

@section('content')

<h2>Counsellor Index</h2>

<section class="search">
    <div class="search-index__anchors">
        <div>
            <h3><a name="top"></a>Counties</h3>
            <ul>
                @foreach($counties as $county)
                    <li><a href="#{{ $county->slug }}">{{ $county->name }}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
    
    <div class="search-index__listing">
        <section>
            @foreach($counties as $county)
                <div class="search-index__county">
                    <h3><a href="{{ route('search.results', $county->slug) }}" name="{{ $county->slug }}">{{ $county->name }}</a>
                        <a class="search-index__back-to-top" href="#top">Back to top</a>
                    </h3>
                    <ul>
                        @foreach($county->children as $town)
                            <li><a href="{{ route('search.results', $town->slug) }}">{{ $town->getOriginal('name') }}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endforeach
        </section>
    </div>
</section>
@endsection
