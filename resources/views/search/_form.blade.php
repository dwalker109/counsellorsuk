<div class="searchform">
    {!! Former::vertical_open()->route('search.prepare')->method('POST') !!}
        {!! Former::select('location')->label('I am looking for a counsellor or psychotherapist in')
            ->options($location)
            ->addClass('js-select2-ajax-lookup')
            ->setAttribute('data-select-on-close', 'true')
            ->setAttribute('data-ajax--url', '/api/lookup-location') !!}
        {!! Former::select('distance')->label('Include counsellors up to this many miles from my location')
            ->options(config('counsellorsuk.search.distances'))->value($distance)
            ->setAttribute('data-select-on-close', 'true') !!}
        {!! Former::select('issue')->label('I would like to talk about')->options($issue)
            ->addClass('js-select2-ajax-lookup')
            ->setAttribute('data-ajax--url', '/api/lookup-issue')
            ->setAttribute('data-placeholder', config('counsellorsuk.search.unfiltered.issue.any-issue'))
            ->setAttribute('data-allow-clear', 'true') !!}
        {!! Former::primary_block_submit('Find a counsellor') !!}
    {!! Former::close() !!}
</div>