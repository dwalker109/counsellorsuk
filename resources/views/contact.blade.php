@extends('layouts.app')

@section('title', 'Contact CounsellorsUK')
@section('meta_description', 'Complete this form to send a message directly to us')

@section('content')

<section>
    
    <div class="counsellor__contact-form">

        <h2 class="counsellor__contact-heading">Contact CounsellorsUK</h2>
        
        <p>Complete this form to send a message directly to us. Please ensure 
        your contact details are correct so we can get back to you promptly.</p>
        
        <div class="counsellor__contact-clarify">
            <p>Please note that this contact form is for contacting our 
            organisation. If you would like to contact one of our members, 
            please go to their profile and {{ strtolower($click_verb) }} 
            <strong>Contact via email</strong>.</p>     
        </div>
           
        {!! Former::open() !!}
        
            {!! Former::text('name')->label('Your name') !!}
            {!! Former::text('email')->label('Your email address') !!}
            {!! Former::text('phone')->label('Your phone number') !!}
            {!! Former::inline_radios('contact_preference')->radios([
                    'Email' => ['value' => 'email'], 
                    'Phone' => ['value' => 'phone'], 
                    'Other (details in message)' => ['value' => 'other'],
                ]) !!}
            {!! Former::textarea('message')->label('Your message')->class('no-wysiwyg')
                ->placeholder('Hi!') !!}
                
            <div class="form-group {{ $errors->has('g-recaptcha-response') ? 'has-error' : null }}">
                <div class="former-recaptcha">    
                    {!! Recaptcha::render() !!}
                    @if ($errors->has('g-recaptcha-response'))
                        <span class="help-block">{{ $errors->first('g-recaptcha-response') }}</span>
                    @endif
                </div>
            </div>
                
            <div class="counsellor__contact-buttons">
                <button type="submit">Contact Us</button>
            </div>
        
        {!! Former::close() !!} 

    </div>

    <div class="counsellor__contact-miniprofile">
        <div class="counsellorsuk__contact-details">
            <h2>We’re here to help</h2>
            <p>Please feel free to contact us if you can't find the information you 
            are looking for, would like help in finding a counsellor, or have a 
            question regarding counselling.</p>
            <p>Please be aware that this is not a helpline for telephone 
            counselling, it is an information line to help you find the support you need.</p>
            <p>Our office hours are Monday to Friday, 9am to 5pm.</p>
        </div>
    </div>    

</section>
@endsection
