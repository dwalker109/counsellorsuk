@if (Auth::guest())
    <div class="signup-cta">
        <h4>Are you a qualified counsellor or psycho&shy;therapist?</h4>
        <p><a class="signup-cta__link" href="{{ url('/register') }}">Join CounsellorsUK today</a></p>
        <p><a class="signup-cta__link" href="{{ url('/profile') }}">Member login</a></p>
    </div>
@endif