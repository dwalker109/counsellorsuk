@extends('layouts.email')

@section('email_content')
<p>You can now set / reset your password for CounsellorsUK which will allow you to
manage your member profile:</p>

<p><a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}">{{ $link }}</a></p>

<p>Once you have set your new password you can immediately view and update your
profile so that potential clients can find you in our extensive counsellors and
psychotherapists directory.</p>

<p>Your profile allows you to manage a number of elements including:</p>

<ul>
    <li>Profile image</li>
    <li>Biography</li>
    <li>Qualifications and experience</li>
    <li>Practice locations</li>
    <li>Skills and specialisms</li>
    <li>Contact details</li>
</ul>

@include('emails._help_and_vision')
@endsection