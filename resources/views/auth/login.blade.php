@extends('layouts.app')

@section('title', 'Login to CounsellorsUK')

@section('content')
<section>
    <div class="auth__login-intro">
        <h2>Choose your login method</h2>
    </div>

    <div class="auth__login-container--multi">
        <div data-mh="login-containers">
            <h2 class="auth__heading">Login with a magic link</h2>
            <p>You can email yourself a magic link, which will log you in to your account
                without requiring your CounsellorsUK password. Many people find this
                a really convenient way of logging in, and since only you have access
                to your email, it is secure as well.</p>

            {!! Former::open(route('magic_link.send')) !!}
                {!! Former::text('magic_link_email')->label('Email address') !!}
                {!! Former::actions()
                    ->large_primary_submit('Email my magic link') !!}
            {!! Former::close() !!}
        </div>
    </div>

    <div class="auth__login-container--multi">
        <div data-mh="login-containers">
            <h2 class="auth__heading">Login with a password</h2>
            <p>If you prefer to login by entering your password directly, no problem.
                If you cannot remember or have never set a password, you can 
                <a href="/password/reset">reset it</a>.
            </p>

            {!! Former::open('/login') !!}
                {!! Former::text('email')->label('Email address') !!}
                {!! Former::password('password')->label('Password') !!}
                {!! Former::actions()
                    ->large_primary_submit('Login') !!}
            {!! Former::close() !!}
        </div>
    </div>
</section>
@endsection
