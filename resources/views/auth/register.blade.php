@extends('layouts.app')

@section('title', 'Sign up to CounsellorsUK')

@section('content')
<section>
    
    <div class="auth__register-intro">
        <h2>CounsellorsUK is run by practitioners for practitioners</h2>
        
        <p>Our aim is for CounsellorsUK to record all of the qualified 
        Counsellors and Psychotherapists from the main 
        <a href="{{ route('cms_page', 'professional-body-memberships') }}" 
        target="_blank">UK professional bodies</a> within the UK on one easily searchable register.</p>
    </div>
    
    <div class="auth__register-blurb">
        <h3>Your searchable <strong>free</strong> profile will include:</h3>
        <ul>
            <li>Your photograph, contact details and professional memberships</li>
            <li>A short ({{ config('counsellorsuk.free_profile_about_limit') }} character) biography</li>
            <li>Links to your other websites and social media pages</li>
            <li>Up to two practice addresses, including maps</li>
            <li>Easy to understand information on your skills, services offered 
            and areas of expertise</li>
        </ul>
    </div>
    
    <div class="auth__register-blurb">
        <h3>Upgrade to a <strong>professional</strong> profile for only £49 per year
        for these great additional benefits:</h3>
        <ul>
            <li>Appear <strong>above</strong> free profiles in search results</li>
            <li>Purchase our <strong>CounsellorsUK Insurance</strong> for £49.28 per year (£42.56 for UKCP members, or £20.16 for students)</li>
            <li>Greatly <strong>expand</strong> your biography</li>
            <li>Include information on your <strong>fees</strong> and <strong>appointment</strong> scheduling</li>
            <li>Detail any <strong>concessionary</strong> rates you offer</li>
            <li>Nearly <strong>5&percnt; from each paid listing</strong> goes to our chosen charity, 
            <a href="http://www.youngminds.org.uk/" target="blank">Young Minds</a></li>
        </ul>
    </div>
</section>
    
<section>
    <div class="auth__login-container">
        <div>
            <h2 class="auth__heading">Register</h2>
            <div class="auth__body-membership-confirm">
                <p>
                    By signing up you are confirming that you are a fully insured member
                    of one of our <a href="{{ route('cms_page', 'membership-categories-we-accept') }}"
                    target="_blank" class="normal-link">approved professional bodies</a>.
                </p>
                <p>
                    Before your profile is visible to the public, we ask that you complete your
                    <strong>professional body registration number</strong> and, if applicable, your 
                    <strong>BACP certificate number</strong> in your profile. We will use these to verify you
                    and will notify you once this has been completed.</p>
            </div>
            {!! Former::open() !!}
                {!! Former::text('name')->label('Name') !!}
                {!! Former::text('email')->label('Email address') !!}
                {!! Former::password('password')->label('Password') !!}
                {!! Former::password('password_confirmation')->label('Confirm password') !!}

                <div class="form-group {{ $errors->has('g-recaptcha-response') ? 'has-error' : null }}">
                    <div class="former-recaptcha">    
                        {!! Recaptcha::render() !!}
                        @if ($errors->has('g-recaptcha-response'))
                            <span class="help-block">{{ $errors->first('g-recaptcha-response') }}</span>
                        @endif
                    </div>
                </div>

                {!! Former::actions()
                    ->large_primary_submit('Register')
                    ->large_link('Already a member - sign in', '/login') !!}
            {!! Former::close() !!}
        </div>
    </div>
</section>
@endsection
