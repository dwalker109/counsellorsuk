@extends('layouts.app')

@section('title', 'Reset your password')

@section('content')
<section>
    <div class="auth__login-container">
        <div>
            <h2 class="auth__heading">Reset your password</h2>
            <p class="auth__link-sent">Speak to Dan to get your password reset!</p>
        </div>
    </div>
</section>
@endsection
