@extends('layouts.app')

@section('title', 'Reset your password')

<!-- Main Content -->
@section('content')
<section>
    <div class="auth__login-container">
        <div>
            <h2 class="auth__heading">Reset your password</h2>
            @if (session('status'))
                <p class="auth__link-sent">A password reset link has been emailed to you. 
                Please check your email.</p>
            @else
                {!! Former::open()->action('/password/email') !!}
                {!! Former::text('email')->label('Email address') !!}
                {!! Former::actions()
                    ->large_primary_submit('Send password reset link') !!}
                    {!! Former::close() !!}
            @endif
        </div>
    </div>
</section>
@endsection
