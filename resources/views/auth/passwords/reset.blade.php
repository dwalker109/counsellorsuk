@extends('layouts.app')

@section('title', 'Reset your password')

@section('content')
<section>
    <div class="auth__login-container">
        <div>
            <h2 class="auth__heading">Reset your password</h2>
            {!! Former::open()->action('/password/reset') !!}
                {!! Former::hidden('token')->value($token) !!}
                {!! Former::text('email')->label('Email address')->value($email) !!}
                {!! Former::password('password')->label('Password') !!}
                {!! Former::password('password_confirmation')->label('Confirm password') !!}
                {!! Former::actions()
                    ->large_primary_submit('Reset password') !!}
            {!! Former::close() !!}
        </div>
    </div>
</section>
@endsection
