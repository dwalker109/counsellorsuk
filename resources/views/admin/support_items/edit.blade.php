@extends('admin._container')

@section('title', "Edit $display_name->singular")

@section('body')

{!! Former::open_for_files()->route("admin.{$display_name->snake_case}.update", $item) !!}
{!! Former::populate($item) !!}
	@include('admin.support_items._fields')
{!! Former::close() !!}

@endsection
