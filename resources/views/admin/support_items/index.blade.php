@extends('admin._container')

@section('title', $display_name->plural)

@section('body')

<a href="{{ route("admin.{$display_name->snake_case}.create") }}" class="crud__item--new">Create a new
	{{ strtolower($display_name->singular) }}</a>

@foreach ($items as $item)
	<li class="crud__item">
		<img src="{{ $item->icon }}">
		{{ $item->name }}
		<a href="{{ route("admin.{$display_name->snake_case}.edit", $item) }}">Edit</a>
		<a href="{{ route("admin.{$display_name->snake_case}.destroy", $item) }}" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure?">Delete</a>
	</li>

@endforeach

@endsection
