@extends('admin._container')

@section('title', "Add $display_name->singular")

@section('body')

{!! Former::open_for_files()->route("admin.{$display_name->snake_case}.store") !!}
	@include('admin.support_items._fields')
{!! Former::close() !!}

@endsection
