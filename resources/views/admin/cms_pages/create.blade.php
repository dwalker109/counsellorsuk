@extends('admin._container')

@section('title', 'Add CMS Page')

@section('body')

{!! Former::open()->route('admin.cms_page.store') !!}
	@include('admin.cms_pages._fields')
{!! Former::close() !!}

@endsection
