@extends('admin._container')

@section('title', 'Edit CMS Page')

@section('body')

{!! Former::open()->route('admin.cms_page.update', $cms_page) !!}
{!! Former::populate($cms_page) !!}
	@include('admin.cms_pages._fields')
{!! Former::close() !!}

@endsection
