{{ csrf_field() }}
{!! Former::select('parent_id')->label('Parent')->options($parent_options) !!}
{!! Former::text('title') !!}
{!! Former::textarea('body') !!}
{!! Former::text('meta_description') !!}
{!! Former::select('permission')->label('View Permission')->options($permission_options) !!}
{!! Former::large_primary_submit('Save') !!}