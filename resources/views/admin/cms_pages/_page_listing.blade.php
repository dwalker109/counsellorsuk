<li class="crud__item">{{ $cms_page->title }} [Available to: {{ $cms_page->applied_permission }}]
	<a href="{{ route('cms_page', $cms_page) }}" target="_blank">View</a>
	<a href="{{ route('admin.cms_page.edit', $cms_page) }}">Edit</a>
	<span class="fa fa-ellipsis-h"></span>
	<a href="{{ route('admin.cms_page.move_up', $cms_page) }}">Up</a>
	<a href="{{ route('admin.cms_page.move_down', $cms_page) }}">Down</a>
	<span class="fa fa-ellipsis-h"></span>
	<a href="{{ route('admin.cms_page.destroy', $cms_page) }}" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure?">Delete</a>
</li>

@if ($cms_page->children)
	<ul>
		@foreach($cms_page->children as $cms_page)
			@include('admin.cms_pages._page_listing', $cms_page)
		@endforeach
	</ul>
@endif
