@extends('admin._container')

@section('title', 'CMS Pages')

@section('body')

<a href="{{ route('admin.cms_page.create') }}" class="crud__item--new">Create a new CMS Page</a>

@each('admin.cms_pages._page_listing', $cms_pages, 'cms_page', 'admin._no_items')

@endsection
