@extends('layouts.app')

@section('title', 'Login to CounsellorsUK')

@section('content')
<section>
    <div class="auth__login-container">
        <div>
            <h2 class="auth__heading">Admin Login</h2>
            {!! Former::open() !!}
                {!! Former::text('email')->label('Email address') !!}
                {!! Former::password('password')->label('Password') !!}
                {!! Former::actions()
                    ->large_primary_submit('Login')
                    ->large_link('Forgot Your Password?', 'password/reset') !!}
            {!! Former::close() !!}
        </div>
    </div>
</section>
@endsection
