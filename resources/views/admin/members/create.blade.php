@extends('admin._container')

@section('title', 'Add Counsellor')

@section('body')

<div class="member-create__info">
	<p>Once the form is submitted, you will be logged as the new user <strong>automatically</strong>.</p>
	<p>Two hours after the account is created, they will be emailed a welcome link <strong>automatically</strong>.</p>
</div>

{!! Former::open()->route('admin.member.store') !!}
	{{ csrf_field() }}
	{!! Former::text('name') !!}
	{!! Former::text('email') !!}
	{!! Former::large_primary_submit('Save') !!}
{!! Former::close() !!}

@endsection
