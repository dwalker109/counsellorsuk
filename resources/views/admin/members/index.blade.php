@extends('admin._container')

@section('title', 'Members')

@section('body')

<p><a href="{{ route('admin.member.create') }}" class="crud__item--new"><span class="fa fa-user-plus" aria-hidden="true"></span> Add a new member</a></p>

<div class="member-index__search">
	<div>
		<div>
			{!! Former::open()->method('GET') !!}

				{!! Former::text('search')->label('Name or email')
					->placeholder('Enter a name or email address here to search') !!}

				{!! Former::select('sort')->label('Sort by')
					->options($sort_options->pluck('label')) !!}
				
				{!! Former::actions()
					->large_primary_submit('Search/sort')
					->large_link('Reset', route('admin.member.index')) !!}

			{!! Former::close() !!}
		</div>
	</div>
</div>

{{ $users->links() }}

<table class="table table-striped">
	<thead>
		<th>Details</th>
		<th>Popularity</th>
		<th>Verified</th>
		<th>Live</th>
		<th>Subscription</th>
		<th>Actions</th>
	</thead>
	<tbody>
		@foreach ($users as $user)
			<tr>
				<td>
					<p class="text-info"><strong>{{ $user->name }}</strong></p>
					<p><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></p>
					@foreach($user->profile->addresses as $address)
						<p><strong>{{ $address->location->name }}</strong></p>
					@endforeach
					<p><em>Registration number:</em> {{ $user->profile->registration_number ?: 'None'}}</p>
					<p><em>BACP number:</em> {{ $user->profile->bacp_certificate_number ?: 'None'}}</p>
					<p><em>Body memberships:</em> {{ $user->profile->present()->body_memberships_full ?: 'None' }}</p>
				</td>
				<td>
					<p>View: <strong>{{ $user->profile->popularity->{Statistics::PROFILE_VIEWED} }}</strong></p>
					<p>Search: <strong>{{ $user->profile->popularity->{Statistics::PROFILE_SEARCH_RESULTS} }}</strong></p>
					<p>Contact: <strong>{{ $user->profile->popularity->{Statistics::PROFILE_CONTACTED} }}</strong></p>
					<p><em>Joined:</em> {{ $user->created_at->toFormattedDateString() }}</p>
					<p><em>Last login:</em> {{ $user->last_login_at ? $user->last_login_at->toFormattedDateString() : 'Never' }}</p>
				</td>
				<td>{{ $user->profile->is_verified ? 'Yes' : 'No' }}</td>
				<td>{{ $user->profile->is_live ? 'Yes' : 'No' }}</td>
				<td>
					@if ($user->subscribed())
						Braintree
					@elseif ($user->subscription_override)
						Manual
					@else
						None
					@endif
				</td>
				<td>
					<p class="crud__item"><a href="{{ route('admin.member.impersonate', $user) }}" target="_blank">Login as member</a></p>
					<p class="crud__item"><a href="{{ route('admin.member.toggle_verified', $user) }}">Toggle verified</a></p>
					<p class="crud__item"><a href="{{ route('admin.member.toggle_live', $user) }}">Toggle live</a></p>
					@if ($user->subscribed() === false)
						<p class="crud__item"><a href="{{ route('admin.member.toggle_legacy_subscriber', $user) }}">Toggle manual subscription</a></p>
					@endif
					<p class="crud__item"><a href="{{ route("admin.member.destroy", $user) }}" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure?">Delete this member</a></p>
					
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{{ $users->links() }}

@endsection
