@extends('admin._container')

@section('title', 'Add Body Membership')

@section('body')

{!! Former::open()->route("admin.body_membership.store") !!}
	@include('admin.body_memberships._fields')
{!! Former::close() !!}

@endsection
