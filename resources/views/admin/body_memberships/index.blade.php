@extends('admin._container')

@section('title', 'Body Memberships')

@section('body')

<a href="{{ route("admin.body_membership.create") }}" class="crud__item--new">Create a new Body Membership</a>

<ul>
	@foreach ($body_memberships as $body_membership)
		<li class="crud__item">
			<strong>{{ $body_membership->body }}:</strong> {{ $body_membership->name }}
			<a href="{{ route("admin.body_membership.export", $body_membership) }}">Export Members</a>
			<a href="{{ route("admin.body_membership.edit", $body_membership) }}">Edit</a>
			<a href="{{ route("admin.body_membership.destroy", $body_membership) }}" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure?">Delete</a>
		</li>
	@endforeach
</ul>

@endsection
