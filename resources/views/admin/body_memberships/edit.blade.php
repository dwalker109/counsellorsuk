@extends('admin._container')

@section('title', 'Edit Body Membership')

@section('body')

{!! Former::open()->route('admin.body_membership.update', $body_membership) !!}
{!! Former::populate($body_membership) !!}
	@include('admin.body_memberships._fields')
{!! Former::close() !!}

@endsection
