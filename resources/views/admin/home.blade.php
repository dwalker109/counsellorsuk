@extends('admin._container')

@section('title', 'CounsellorsUK Admin Dashboard')

@section('body')

<h2>Stats</h2>
<ul>
	<li>Live: {{ $stats->get('live') }}</li>
	<li>Paid: {{ $stats->get('paid') }}</li>
	<li>New: {{ $stats->get('new') }}</li>
	<li>New &amp; paid: {{ $stats->get('new_paid') }}</li>
</ul>

<a href="{{ route('admin.export_profiles') }}">Export all profiles as CSV</a>

<h2>Manage</h2>
<ul>
	<li><a href="{{ route('admin.member.index') }}">Manage Members</a>
	<li><a href="{{ route('admin.cms_page.index') }}">Manage CMS Pages</a>
	<li><a href="{{ route('admin.body_membership.index') }}">Manage Body Memberships</a>
	<li><a href="{{ route('admin.approach.index') }}">Manage Approaches</a>
	<li><a href="{{ route('admin.issue.index') }}">Manage Issues</a>
	<li><a href="{{ route('admin.other_service.index') }}">Manage Other Services</a>
	<li><a href="{{ route('admin.skill.index') }}">Manage Skills</a>
</ul>

@endsection
