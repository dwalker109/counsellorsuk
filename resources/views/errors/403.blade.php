@extends('layouts.app')

@section('title', 'Sorry, you are not authorised to view that page')

@section('content')

<section>
    <div class="error_main">
        <h2>Sorry, you are not authorised to view that page</h2>
        <p>Please <a href="{{ url('/login') }}">login</a> or 
        <a href="{{ url('/register') }}">sign up</a> for an account.</p>
    </div>
</section>

@endsection
