@extends('layouts.app')

@section('title', "Sorry, we can't find that page")

@section('content')

<section>
    <div class="error_main">
        <h2>Sorry, we can't find that page</h2>
        <p>You might have followed an outdated bookmark or external link. You
        will hopefully be able to find the page you are looking for from our
        <a href="{{ route('home') }}">homepage</a>.</p>
    </div>
</section>

@endsection
