$(document).ready(function() {

	// Homepage
	$('.homepage').backstretch();

	// Init Bootstrap popovers and tooltips
	$('[data-toggle="popover"]').popover()
	$('[data-toggle="tooltip"]').tooltip()

	// WYSIWG editor
	$('textarea').not('.no-wysiwyg').not('.g-recaptcha-response').summernote({
		height: 200,
		maxHeight: 600,
		toolbar: [
			['style', ['style']],
			['para', ['ol', 'ul', 'paragraph']],
			['font', ['bold', 'italic', 'clear']],
			['misc', ['fullscreen', 'codeview', 'undo', 'redo']],
		],
		dialogsInBody: true
	});

	// Better select boxes
	$.fn.select2.defaults.set('theme', 'bootstrap')
	$('select').attr('style', 'width: 100%');
	$('select').select2();

	// Search reset
	$('.js-search-reset').click(function(e) {
		e.preventDefault();
		$('#issue').select2('val', '');
	});

	// AJAX lookups
	$('.js-select2-ajax-lookup').select2({
		ajax: {
			// URL is set via data-ajax--url in the markup
			url: false,
			delay: 150,
			cache: true,
			// Send term and paging data
			data: function (params) {
				return {
					term: params.term, // search term
					page: params.page
				};
		    },
			// Transform results into Select2 format
			processResults: function (data, params) {
				params.page = params.page || 1;
	            return {
	                results: data.data.map(function (item) {
	                    return {
	                    	// The search page will use the slug, not an ID
	                        id: item.slug,
	                        text: item.name,
	                    }
	                }),
	                pagination: {
						more: data.next_page_url
                    }
	            }
	        },
		},
		minimumInputLength: 2,
		maximumInputLength: 20,
	});

	// AJAX lookups for profile editing
	$('.js-select2-ajax-lookup-for-profile').select2({
		ajax: {
			// URL is set via data-ajax--url in the markup
			url: false,
			delay: 150,
			cache: true,
			// Send term and paging data
			data: function (params) {
				return {
					term: params.term, // search term
					page: params.page
				};
		    },
			// Transform results into Select2 format
			processResults: function (data, params) {
				params.page = params.page || 1;
	            return {
	                results: data.data.map(function (item) {
	                    return {
	                        id: item.id,
	                        text: item.name,
	                    }
	                }),
	                pagination: {
						more: data.next_page_url
                    }
	            }
	        },
		},
		minimumInputLength: 2,
		maximumInputLength: 20,
	});

	// Auto submit a Select2 when item is selected
	$('select.js-select2-submit-on-select').on("select2:select", function (event) {
		$(this).parents('form').submit();
		
		// Disable UI after submitting
		$(this).parents('form').children('input').prop('disabled', true);
		$(this).parents('form').children('input').addClass('disabled');
	});

	// Ensure disabled controls from above are enabled (i.e. back was clicked)
	$('input.js-select2-submit-on-select').prop('disabled', false);
	$('input.js-select2-submit-on-select').removeClass('disabled');
	
	// Braintree UI
	if ($('#braintree-container').length) {

		// Initialise checkout UI with token
		$.get('/braintree/initialise', function(data) {
			braintree.setup(data.token, data.ui, {
				container: data.container
			});
		});
	}

	// Profile additional address UI initial state
	$('.js-additional-address').each(function() {
		var el = $(this);
		if (el.attr('value') == 0 && el.prop('checked')) {
			$('#address-' + el.data('address-index')).toggle('fast');
		}
		
	});

	// Profile additional address UI click
	$('.js-additional-address').click(function(e) {
		var el = $(this);
		if (el.attr('value') == 0) {
			$('#address-' + el.data('address-index')).hide('fast');
		} else {
			$('#address-' + el.data('address-index')).show('fast');
		}
	})
	
	// Workaround for Bootstrap nav dropdown not working on mobile
	$('.dropdown-toggle').click(function(e) {
		e.preventDefault();
		setTimeout($.proxy(function() {
			if ('ontouchstart' in document.documentElement) {
				$(this).siblings('.dropdown-backdrop').off().remove();
			}
		}, this), 0);
	});

	// Load CMS bootstrap modals
	$('.js-cms-modal').click(function (e) {
		e.preventDefault();
		$.get($(this).attr('href'), function(data) {
			$('#cms-modal .modal-body').html(data);
			$('#cms-modal').modal();
		});
	});
});
