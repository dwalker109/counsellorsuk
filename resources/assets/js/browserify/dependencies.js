// Load dependencies via Node / Browserify
var $ = global.$ = global.jQuery = require('jquery');
var bootstrap_sass = require('bootstrap-sass');
var select2 = require('select2');
var summernote = require('summernote');
var braintree = global.braintree = require('braintree-web');
var jquery_match_height = require('jquery-match-height');
var backstretch = require('jquery.backstretch');