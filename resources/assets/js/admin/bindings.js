$(document).ready(function() {

	// Better select boxes
	$('select').attr('style', 'width: 100%');
	$('select').select2({
		theme: "bootstrap"
	});

	// WYSIWG editor
	$('textarea').summernote({
		toolbar: [
			['style', ['style']],
			['para', ['ol', 'ul', 'paragraph']],
			['font', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
			['insert', ['picture', 'link', 'video', 'table', 'hr']],
			['misc', ['fullscreen', 'codeview', 'undo', 'redo']],
		],
		dialogsInBody: true
	});

});
