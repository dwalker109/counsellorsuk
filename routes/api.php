<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::match(['get', 'post'], '/lookup-location', 'ApiController@lookupLocation');
Route::match(['get', 'post'], '/lookup-town', 'ApiController@lookupTown');
Route::match(['get', 'post'], '/lookup-issue', 'ApiController@lookupIssue');
Route::match(['get', 'post'], '/lookup-approach', 'ApiController@lookupApproach');

