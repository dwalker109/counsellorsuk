<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

// Static and CMS pages
Route::get('/', 'CmsPageController@home')->name('home');
Route::get('/info/{cms_page}', 'CmsPageController@cms')->name('cms_page');
Route::get('/contact', 'CmsPageController@contact')->name('contact');
Route::post('/contact', 'CmsPageController@contactSend')->name('contact.send');

// Auth routes for regular users
Route::auth();

// Magic links
Route::post('/magic-link', 'Auth\MagicLinkController@send')->name('magic_link.send');
Route::get('/magic-link/{magic_link_token}', 'Auth\MagicLinkController@process')->name('magic_link.process');

// Counsellor view, contact, claim
Route::get('/counsellors/{profile}', 'CounsellorController@view')->name('counsellor');
Route::get('/counsellors/{profile}/contact', 'CounsellorController@contact')->name('counsellor.contact');
Route::post('/counsellors/{profile}/contact', 'CounsellorController@contactSend')->name('counsellor.contact.send');
Route::get('/counsellors/{profile}/claim', 'CounsellorController@claimProfile')->name('profile.claim');
Route::post('/counsellors/{profile}/claim', 'CounsellorController@claimProfileSend')->name('profile.send_claim');

// Counsellor index and search
Route::get('/counsellor-index', 'SearchController@index')->name('search.index');
Route::post('/search', 'SearchController@prepare')->name('search.prepare');
Route::get('/search/{location}/{distance?}/{issue?}', 'SearchController@results')->name('search.results');

// Admins Dashboard
Route::group(['namespace' => 'Admin', 'prefix' => '/admin', 'as' => 'admin.'], function () {

    // Add auth routes (admin middleware isn't applied to these)
    Route::get('/login', 'LoginController@showLoginForm')->name('login');
    Route::post('/login', 'LoginController@login')->name('do_login');
    Route::get('/logout', 'LoginController@logout')->name('logout');
    Route::get('/password/{any?}', function () {
        return view('auth.passwords.reset_admin');
    })->name('passwords');

    Route::group(['middleware' => 'auth.admin'], function () {

        // Admin homepage
        Route::get('/', 'HomeController@index')->name('home');
        Route::get('/export-profiles', 'HomeController@exportProfiles')->name('export_profiles');

        // Manage members
        Route::group(['prefix' => 'member'], function () {
            Route::get('/', 'MemberController@index')->name('member.index');
            Route::get('/create', 'MemberController@create')->name('member.create');
            Route::post('/', 'MemberController@store')->name('member.store');
            Route::get('/{user}/impersonate', 'MemberController@impersonate')->name('member.impersonate');
            Route::get('/{user}/legacy-subscriber', 'MemberController@toggleLegacySubscriber')
                ->name('member.toggle_legacy_subscriber');
            Route::get('/{user}/verified', 'MemberController@toggleVerified')->name('member.toggle_verified');
            Route::get('/{user}/live', 'MemberController@toggleLive')->name('member.toggle_live');
            Route::delete('/{user}', 'MemberController@destroy')->name('member.destroy');
        });

        // CMS
        Route::get('cms_page/{cms_page}/move-up', 'CmsPageController@moveUp')->name('cms_page.move_up');
        Route::get('cms_page/{cms_page}/move-down', 'CmsPageController@moveDown')->name('cms_page.move_down');
        Route::resource('cms_page', 'CmsPageController');

        // Support items
        Route::resource('approach', 'ApproachController');
        Route::resource('issue', 'IssueController');
        Route::resource('other_service', 'OtherServiceController');
        Route::resource('skill', 'SkillController');

        // Body memberships
        Route::resource('body_membership', 'BodyMembershipController');
        Route::get('body_membership/{body_membership}/export', 'BodyMembershipController@export')->name('body_membership.export');
    });
});

// Members Dashboard
Route::group(['namespace' => 'Members'], function () {

    // Process email verify (no auth required)
    Route::get('/verify-email/{email_verify_token}', 'ProfileController@processVerifyEmail')->name('verify.process');

    Route::group(['middleware' => 'auth'], function () {

        // Dashboard
        Route::get('/dashboard', function () {
            return redirect()->route('profile.view');
        })->name('dashboard');

        // Send verify email
        Route::get('/verify-email', 'ProfileController@sendVerifyEmail')->name('verify.send');

        // Member Benefits
        Route::get('/member-benefits', 'ProfileController@memberBenefits')->name('member_benefits');

        // Change password
        Route::get('/change-password', 'ProfileController@viewPassword')->name('password.view');
        Route::post('/change-password', 'ProfileController@updatePassword')->name('password.update');

        // Subscriptions
        Route::group(['prefix' => '/subscribe', 'middleware' => 'sub_editable'], function () {
            Route::get('/', 'SubscriptionController@index')->name('subscription.view');
            Route::post('/', 'SubscriptionController@newSubscription')->name('subscription.new');
            Route::post('/swap', 'SubscriptionController@swap')->name('subscription.swap');
            Route::post('/cancel', 'SubscriptionController@cancel')->name('subscription.cancel');
            Route::post('/resume', 'SubscriptionController@resume')->name('subscription.resume');
            Route::post('/update-card', 'SubscriptionController@updateCard')->name('subscription.update_card');
        });

        // Subscription Middleware will redirect here if not editable
        Route::get('/subscription-not-editable', function () {
            return view('members.subscription_not_editable');
        })->name('subscription.not_editable');

        // Profile Management
        Route::group(['prefix' => '/profile'], function () {
            Route::get('/', 'ProfileController@index')->name('profile.view');
            Route::get('/photo', 'ProfileController@viewPhoto')->name('profile.photo');
            Route::post('/photo', 'ProfileController@updatePhoto')->name('profile.update_photo');
            Route::delete('/photo', 'ProfileController@deletePhoto')->name('profile.delete_photo');
            Route::get('/details', 'ProfileController@viewDetails')->name('profile.details');
            Route::post('/details', 'ProfileController@updateDetails')->name('profile.update_details');
            Route::get('/contact', 'ProfileController@viewContact')->name('profile.contact');
            Route::post('/contact', 'ProfileController@updateContact')->name('profile.update_contact');
            Route::get('/addresses', 'ProfileController@viewAddresses')->name('profile.addresses');
            Route::post('/addresses', 'ProfileController@updateAddresses')->name('profile.update_addresses');
            Route::get('/practice', 'ProfileController@viewPractice')->name('profile.practice');
            Route::post('/practice', 'ProfileController@updatePractice')->name('profile.update_practice');
            Route::get('/counselling', 'ProfileController@viewCounselling')->name('profile.counselling');
            Route::post('/counselling', 'ProfileController@updateCounselling')->name('profile.update_counselling');
    
            Route::group(['middleware' => 'profile_deletable'], function () {
                Route::get('/delete', 'ProfileController@viewDelete')->name('profile.delete');
                Route::delete('/delete', 'ProfileController@doDelete')->name('profile.do_delete');
            });

            // Profile Deletable Middleware will redirect here if not deletable
            Route::get('/not-deletable', function () {
                return view('members.profile_not_deletable');
            })->name('profile.not_deletable');
        });
    });
});

// Braintree
Route::get('braintree/initialise', function () {
    if ($braintree_id = Auth::user()->braintree_id) {
        $tokenOptions = ['customerId' => $braintree_id];
    } else {
        $tokenOptions = [];
    }

    return [
        'token' => Braintree\ClientToken::generate($tokenOptions),
        'ui' => 'dropin',
        'container' => 'braintree-container',
    ];
})->middleware('auth');

// Braintree webhooks
Route::post('braintree/webhook', '\Laravel\Cashier\Http\Controllers\WebhookController@handleWebhook');

// Sitemap
Route::get('sitemap.xml', 'SitemapController@index')->name('sitemap');
