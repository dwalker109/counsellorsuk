var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    mix.browserify('resources/assets/js/browserify/dependencies.js');
  	mix.copy('node_modules/font-awesome/fonts', 'public/build/fonts');
  	mix.copy('node_modules/summernote/dist/summernote.css', '/tmp/_summernote.scss');
  	mix.copy('node_modules/summernote/dist/font', 'public/build/css/font')

    mix.sass('app.scss').sass('admin.scss');

  	mix.copy('resources/assets/img', 'public/build/img');

    mix.scriptsIn('resources/assets/js/app', 'public/js/app.js');
    mix.scriptsIn('resources/assets/js/admin', 'public/js/admin.js');
    mix.version(['css/app.css', 'css/admin.css', 'js/app.js', 'js/admin.js', 'js/dependencies.js']);
    
});
