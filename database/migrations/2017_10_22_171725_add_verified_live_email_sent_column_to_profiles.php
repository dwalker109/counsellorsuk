<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVerifiedLiveEmailSentColumnToProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->boolean('verified_live_email_sent')->after('is_live');
        });

        // Update existing DB records (do not save models since we don't want to send any emails)
        \DB::table('profiles')
            ->where('is_verified', true)
            ->where('is_live', true)
            ->update(['verified_live_email_sent' => true]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->dropColumn('verified_live_email_sent');
        });
    }
}
