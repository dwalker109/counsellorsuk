<?php

use CounsellorsUK\Models\Location;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveLondonBoroughSuffixAndAddTownsToAllLocationSlugs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (Location::where('name', 'LIKE', '% - London Borough')->cursor() as $location) {
            $location->name = str_replace(' - London Borough', '', $location->getOriginal('name'));
            $location->save();
        }

        foreach (Location::cursor() as $location) {
            $location->slug = null; // Force re-sluggify on save
            $location->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Cannot revert
    }
}
