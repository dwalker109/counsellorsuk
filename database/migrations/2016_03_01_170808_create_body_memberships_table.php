<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBodyMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('body_memberships', function (Blueprint $table) {
            $table->increments('id');
            $table->string('body');
            $table->string('name');
            $table->boolean('is_supervisory');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('body_memberships');
    }
}
