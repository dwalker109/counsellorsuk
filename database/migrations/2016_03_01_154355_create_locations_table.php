<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_location_id')->unsigned()->nullable();
            $table->string('name');
            $table->enum('type', ['town', 'county', 'postcode']);
            $table->timestamps();

            $table->foreign('parent_location_id')
                ->references('id')
                ->on('locations');
        });

        // Manually add latlng as MySQL POINT
        DB::statement('ALTER TABLE locations ADD geo POINT AFTER type');
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->dropForeign('locations_parent_location_id_foreign');
        });

        Schema::drop('locations');
    }
}
