<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApproachProfileTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('approach_profile', function (Blueprint $table) {
            $table->integer('profile_id')->unsigned();
            $table->integer('approach_id')->unsigned();

            $table->foreign('profile_id')
                ->references('id')
                ->on('profiles')
                ->onDelete('cascade');

            $table->foreign('approach_id')
                ->references('id')
                ->on('approaches')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('approach_profile', function (Blueprint $table) {
            $table->dropForeign('approach_profile_profile_id_foreign');
        });

        Schema::table('approach_profile', function (Blueprint $table) {
            $table->dropForeign('approach_profile_approach_id_foreign');
        });

        Schema::drop('approach_profile');
    }
}
