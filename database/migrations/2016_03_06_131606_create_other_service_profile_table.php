<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtherServiceProfileTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('other_service_profile', function (Blueprint $table) {
            $table->integer('profile_id')->unsigned();
            $table->integer('other_service_id')->unsigned();

            $table->foreign('profile_id')
                ->references('id')
                ->on('profiles')
                ->onDelete('cascade');

            $table->foreign('other_service_id')
                ->references('id')
                ->on('other_services')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('other_service_profile', function (Blueprint $table) {
            $table->dropForeign('other_service_profile_profile_id_foreign');
        });

        Schema::table('other_service_profile', function (Blueprint $table) {
            $table->dropForeign('other_service_profile_other_service_id_foreign');
        });

        Schema::drop('other_service_profile');
    }
}
