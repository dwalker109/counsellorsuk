<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileSkillTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('profile_skill', function (Blueprint $table) {
            $table->integer('profile_id')->unsigned();
            $table->integer('skill_id')->unsigned();

            $table->foreign('profile_id')
                ->references('id')
                ->on('profiles')
                ->onDelete('cascade');

            $table->foreign('skill_id')
                ->references('id')
                ->on('skills')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('profile_skill', function (Blueprint $table) {
            $table->dropForeign('profile_skill_profile_id_foreign');
        });

        Schema::table('profile_skill', function (Blueprint $table) {
            $table->dropForeign('profile_skill_skill_id_foreign');
        });

        Schema::drop('profile_skill');
    }
}
