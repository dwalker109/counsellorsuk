<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('profile_id')->unsigned();
            $table->string('name');
            $table->string('address_1');
            $table->string('address_2')->nullable();
            $table->string('postal_code');
            $table->integer('location_id')->unsigned();
            $table->boolean('show_full_address');
            $table->boolean('is_primary');
            $table->timestamps();

            $table->foreign('profile_id')
                ->references('id')
                ->on('profiles')
                ->onDelete('cascade');

            $table->foreign('location_id')
                ->references('id')
                ->on('locations');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->dropForeign('addresses_profile_id_foreign');
            $table->dropForeign('addresses_location_id_foreign');
        });

        Schema::drop('addresses');
    }
}
