<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeEmailVerifyTokenColumnToTextOnUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('email_verify_token');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->text('email_verify_token')->after('remember_token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('email_verify_token');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('email_verify_token', 100)->after('remember_token')->nullable();
        });
    }
}
