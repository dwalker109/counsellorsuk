<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->boolean('is_insured');
            $table->boolean('is_verified');
            $table->boolean('is_live');
            $table->tinyInteger('stage_bitmask')->unsigned();
            $table->string('registration_number');
            $table->string('main_qualification');
            $table->text('about');
            $table->json('counselling_hours');
            $table->json('phone_numbers');
            $table->json('urls');
            $table->json('social_media');
            $table->json('appointments_at');
            $table->json('session_lengths');
            $table->json('fees');
            $table->boolean('probono_available');
            $table->text('probono_about');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->dropForeign('profiles_user_id_foreign');
        });

        Schema::drop('profiles');
    }
}
