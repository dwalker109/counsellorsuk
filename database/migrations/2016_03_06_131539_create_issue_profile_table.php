<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssueProfileTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('issue_profile', function (Blueprint $table) {
            $table->integer('profile_id')->unsigned();
            $table->integer('issue_id')->unsigned();

            $table->foreign('profile_id')
                ->references('id')
                ->on('profiles')
                ->onDelete('cascade');

            $table->foreign('issue_id')
                ->references('id')
                ->on('issues')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('issue_profile', function (Blueprint $table) {
            $table->dropForeign('issue_profile_profile_id_foreign');
        });

        Schema::table('issue_profile', function (Blueprint $table) {
            $table->dropForeign('issue_profile_issue_id_foreign');
        });

        Schema::drop('issue_profile');
    }
}
