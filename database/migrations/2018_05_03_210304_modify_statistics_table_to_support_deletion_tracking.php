<?php

use CounsellorsUK\Models\Statistic;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyStatisticsTableToSupportDeletionTracking extends Migration
{
    public function __construct()
    {
        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('statistics', function (Blueprint $table) {
            DB::statement("ALTER TABLE statistics CHANGE tag tag ENUM('profile_viewed', 'profile_search_results', 'profile_contacted', 'profile_deleted')");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Statistic::where('tag', 'profile_deleted')->delete();

        Schema::table('statistics', function (Blueprint $table) {
            DB::statement("ALTER TABLE statistics CHANGE tag tag ENUM('profile_viewed', 'profile_search_results', 'profile_contacted')");
        });
    }
}
