<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBodyMembershipProfileTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('body_membership_profile', function (Blueprint $table) {
            $table->integer('profile_id')->unsigned();
            $table->integer('body_membership_id')->unsigned();

            $table->foreign('profile_id')
                ->references('id')
                ->on('profiles')
                ->onDelete('cascade');

            $table->foreign('body_membership_id')
                ->references('id')
                ->on('body_memberships')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('body_membership_profile', function (Blueprint $table) {
            $table->dropForeign('body_membership_profile_profile_id_foreign');
        });

        Schema::table('body_membership_profile', function (Blueprint $table) {
            $table->dropForeign('body_membership_profile_body_membership_id_foreign');
        });

        Schema::drop('body_membership_profile');
    }
}
