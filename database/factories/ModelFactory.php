<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(CounsellorsUK\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('secret'),
        'remember_token' => str_random(10),
        'verified' => $faker->randomElement([true, false]),
    ];
});

$factory->define(CounsellorsUK\Models\Profile::class, function (Faker\Generator $faker) {
    return [
        'is_insured' => $faker->randomElement([true, false]),
        'is_verified' => $faker->randomElement([true, false]),
        'is_live' => $faker->randomElement([true, false]),
        'registration_number' => $faker->randomNumber(5),
        'main_qualification' => $faker->sentence,
        'about' => "<h1>{$faker->sentence}</h1><p>{$faker->text}</p>",
        'counselling_hours' => [
            'total' => $faker->randomNumber(4),
            'years' => $faker->randomDigitNotNull,
        ],
        'phone_numbers' => [
            'primary' => $faker->phoneNumber,
            'additional' => $faker->phoneNumber,
        ],
        'urls' => [
            'primary' => $faker->url,
            'additional' => $faker->url,
        ],
        'social_media' => [
            'facebook' => $faker->url,
            'twitter' => $faker->url,
            'linkedin' => $faker->url,
        ],
        'appointments_at' => [
            'weekdays' => $faker->randomElement([true, false]),
            'some_evenings' => $faker->randomElement([true, false]),
            'weekends' => $faker->randomElement([true, false]),
        ],
        'session_lengths' => [
            'initial' => $faker->randomNumber(2),
            'subsequent' => $faker->randomNumber(2),
        ],
        'fees' => [
            'adults' => $faker->randomNumber(2),
            'adolescents' => $faker->randomNumber(2),
            'families' => $faker->randomNumber(2),
            'couples' => $faker->randomNumber(2),
            'children' => $faker->randomNumber(2),
            'concessions' => $faker->randomNumber(2),
        ],
        'probono_available' => $faker->randomElement([true, false]),
        'probono_about' => $faker->sentence,
    ];
});

$factory->define(CounsellorsUK\Models\Admin::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(CounsellorsUK\Models\Approach::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->catchPhrase,
    ];
});

$factory->define(CounsellorsUK\Models\Issue::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->catchPhrase,
    ];
});

$factory->define(CounsellorsUK\Models\OtherService::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->catchPhrase,
    ];
});

$factory->define(CounsellorsUK\Models\Skill::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->catchPhrase,
    ];
});

$factory->define(CounsellorsUK\Models\BodyMembership::class, function (Faker\Generator $faker) {
    return [
        'body' => $faker->randomElement(['FOO', 'BAR', 'BAZ', 'ACK']),
        'name' => $faker->company,
        'is_supervisory' => $faker->randomElement([true, false]),
    ];
});

$factory->define(CounsellorsUK\Models\Location::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->city,
        'type' => $faker->randomElement(['town', 'county', 'postcode']),
        'geo' => $faker->longitude(-5, 5).','.$faker->latitude(50, 60),
    ];
});

$factory->define(CounsellorsUK\Models\CmsPage::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'body' => "<h1>{$faker->sentence}</h1><p>{$faker->text}</p>",
        'meta_description' => $faker->sentence,
    ];
});

$factory->define(CounsellorsUK\Models\Address::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->streetName,
        'address_1' => $faker->streetAddress,
        'postal_code' => $faker->postcode,
        'location_id' => rand(1, 100),
    ];
});
