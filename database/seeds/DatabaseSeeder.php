<?php

use CounsellorsUK\Models\Address;
use CounsellorsUK\Models\Admin;
use CounsellorsUK\Models\Approach;
use CounsellorsUK\Models\BodyMembership;
use CounsellorsUK\Models\CmsPage;
use CounsellorsUK\Models\Issue;
use CounsellorsUK\Models\Location;
use CounsellorsUK\Models\OtherService;
use CounsellorsUK\Models\Profile;
use CounsellorsUK\Models\Skill;
use CounsellorsUK\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(Admin::class, 3)->create();
        factory(Approach::class, 20)->create();
        factory(Issue::class, 20)->create();
        factory(OtherService::class, 20)->create();
        factory(Skill::class, 20)->create();
        factory(BodyMembership::class, 20)->create();
        factory(Location::class, 300)->create();
        factory(CmsPage::class, 20)->create();
        User::unsetEventDispatcher();
        factory(User::class, 100)->create()->each(function ($u) {
            $u->profile()->save(factory(Profile::class)->make());
            // Manually build a fake slug since events are not firing
            $u->profile->slug = uniqid();
            $u->profile->save();
            $u->profile->approaches()->sync(array_rand(array_flip(range(1, 20)), 2));
            $u->profile->issues()->sync(array_rand(array_flip(range(1, 20)), 2));
            $u->profile->other_services()->sync(array_rand(array_flip(range(1, 20)), 2));
            $u->profile->skills()->sync(array_rand(array_flip(range(1, 20)), 2));
            $u->profile->body_memberships()->sync(array_rand(array_flip(range(1, 20)), 2));
            $u->profile->addresses()->save(factory(Address::class)->make());
        });
    }
}
