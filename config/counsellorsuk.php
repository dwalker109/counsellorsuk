<?php

return [

    /*
    |--------------------------------------------------------------------------
    | CounsellorsUK
    |--------------------------------------------------------------------------
    |
    | Various domain specific settings
    |
    */
    
    // Email account to receive contact forms, notifications etc.
    // Do not use for BCCs, they break SparkPost!
    'notification_email' => [
        'address' => 'ej@counsellors-uk.org',
        'name' => 'Emma'
    ],
    
    // Allow sending contact mails to profiles without a verified email address?
    'allow_contact_to_unverified_email' => true,
    
    // How many seconds should the contact a counsellor followup email be scheduled for?
    'contact_followup_delay' => 24 * 60 * 60, // Hours * Minutes * Seconds
    
    // How many seconds should the selected signup email intro be scheduled for?
    'selected_signup_welcome_delay' => 2 * 60 * 60, // Hours * Minutes * Seconds
    
    // How many characters of 'about' to display on free profiles
    'free_profile_about_limit' => 1000,

    // Various profile config values
    'profile' => [

        // Stages - changing these will require DB updates as well
        'stages' => [
            (object) [
                'number' => CounsellorsUK\Models\Profile::PHOTO,
                'name' => 'Photo',
                'description' => 'Your profile photo',
                'route' => 'profile.photo',
                'fa_icon' => 'fa-photo',
            ],
            (object) [
                'number' => CounsellorsUK\Models\Profile::DETAILS,
                'name' => 'Details',
                'description' => 'Professional and biography details',
                'route' => 'profile.details',
                'fa_icon' => 'fa-certificate',
            ],
            (object) [
                'number' => CounsellorsUK\Models\Profile::CONTACT,
                'name' => 'Contact information',
                'description' => 'Name, email address and other contact details',
                'route' => 'profile.contact',
                'fa_icon' => 'fa-at',
            ],
            (object) [
                'number' => CounsellorsUK\Models\Profile::ADDRESSES,
                'name' => 'Addresses',
                'description' => 'Addresses at which your practice operates',
                'route' => 'profile.addresses',
                'fa_icon' => 'fa-compass',
            ],
            (object) [
                'number' => CounsellorsUK\Models\Profile::PRACTICE,
                'name' => 'Practice',
                'description' => 'Practice hours and fees',
                'route' => 'profile.practice',
                'fa_icon' => 'fa-book',
                ],
            (object) [
                'number' => CounsellorsUK\Models\Profile::COUNSELLING,
                'name' => 'Counselling',
                'description' => 'How you counsel: your approach, issues you work with, your skills, services available',
                'route' => 'profile.counselling',
                'fa_icon' => 'fa-comments',
            ],
        ],

        // Tooltip strings for fees
        'fees' => [
            'adults' => 'Adults are aged 18 years and over',
            'adolescents' => 'Adolescents are aged 13 to 17 years',
            'children' => 'Children are aged up to 12 years',
        ],
        
        // Where to save profile photos, relative to /storage
        'photo_path_root' => '/profiles/',

        // Additional photo sizes
        'photo_sizes' => [
            'large' => ['width' => 600, 'height' => 800],
            'medium' => ['width' => 300, 'height' => 400],
            'thumbnail' => ['width' => 150, 'height' => 200],
        ],

        // Path to 'not found' image
        'photo_not_found' => base_path().'/resources/assets/img/profile-not-found.png',

    ],

    'support_items' => [

        // Where to save icons, relative to /storage
        'icon_path_root' => '/icons/',

        // Icon size
        'icon_size' => ['width' => 48, 'height' => 44],

        // Path to 'not found' icon
        'icon_not_found' => base_path().'/resources/assets/img/icon-not-found.png',

    ],

    'search' => [

        // URL fragments to use to indicate that a search option is to be unfiltered/defaulted
        'unfiltered' => [
            'location' => ['location-not-selected' => 'Please choose a location'],
            'distance' => ['within-20-miles' => '20'],
            'issue' => ['any-issue' => 'Any issue'],
        ],

        // Search distance options
        'distances' => [
            'within-5-miles' => '5',
            'within-10-miles' => '10',
            'within-20-miles' => '20',
            'within-30-miles' => '30',
            'within-40-miles' => '40',
            'within-50-miles' => '50',
        ],
        
        // Member sort options for admin search
        'admin_member_sort_options' => [
            [
                'label' => 'Name',
                'column' => 'name',
                'order' => 'asc',
            ],
            [
                'label' => 'Email address',
                'column' => 'email',
                'order' => 'asc',
            ],
            [
                'label' => 'Member since (newest first)',
                'column' => 'created_at',
                'order' => 'desc',
            ],
            [
                'label' => 'Last login (newest first)',
                'column' => 'last_login_at',
                'order' => 'desc',
            ],
        ],

    ],

];
