<?php

namespace CounsellorsUK\Providers;

use Agent;
use Braintree\Configuration as Braintree_Configuration;
use Cache;
use CounsellorsUK\Models\CmsPage;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;
use Laravel\Cashier\Cashier;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        // Braintree SDK and Laravel Cashier config
        Braintree_Configuration::environment(env('BRAINTREE_ENV'));
        Braintree_Configuration::merchantId(env('BRAINTREE_MERCHANT_ID'));
        Braintree_Configuration::publicKey(env('BRAINTREE_PUBLIC_KEY'));
        Braintree_Configuration::privateKey(env('BRAINTREE_PRIVATE_KEY'));
        Cashier::useCurrency('gbp', '£');
        
        // Load custom validation rules
        Validator::extend(
            'enforce_json_structure',
            'CounsellorsUK\Helpers\CustomValidationRules@enforceJsonStructure'
        );

        Validator::extend(
            'is_current_password',
            'CounsellorsUK\Helpers\CustomValidationRules@isCurrentPassword'
        );

        // Compose the search form defaults - the form is used as a partial, so
        // each parent view it appears on needs to be specified here
        view()->creator(['home', 'search.index', 'cms_page.*'], function ($view) {
            $view->with([
                'location' => config('counsellorsuk.search.unfiltered.location'),
                'distance' => key(config('counsellorsuk.search.unfiltered.distance')),
                'issue' => config('counsellorsuk.search.unfiltered.issue'),
                'approach' => config('counsellorsuk.search.unfiltered.approach'),
            ]);
        });

        // Add header page listings and user agent based click verb to all views
        view()->composer('*', function ($view) {
            $about = Cache::remember('about', 1, function () {
                $page = CmsPage::where('menu_handle', 'about')->first();
                return $page
                  ? $page->children()->defaultOrder()->get()
                  : [];
            });

            $therapies = Cache::remember('therapies', 1, function () {
                $page = CmsPage::where('menu_handle', 'therapies')->first();
                return $page
                  ? $page->children()->defaultOrder()->get()
                  : [];
            });

            $what_concerns_you = Cache::remember('what_concerns_you', 1, function () {
                $page = CmsPage::where('menu_handle', 'issues')->first();
                return $page
                  ? $page->children()->defaultOrder()->get()
                  : [];
            });

            $counsellors_area = Cache::remember('counsellors_area', 1, function () {
                $page = CmsPage::where('menu_handle', 'counsellors_area')->first();
                return $page
                  ? $page->children()->defaultOrder()->get()
                  : [];
            });

            $view->with([
                'about' => $about,
                'therapies' => $therapies,
                'what_concerns_you' => $what_concerns_you,
                'counsellors_area' => $counsellors_area,
                'click_verb' => Agent::isDesktop() ? 'Click' : 'Tap',
            ]);
        });
    }

    /**
     * Register any application services.
     */
    public function register()
    {
        //
    }
}
