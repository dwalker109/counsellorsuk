<?php

namespace CounsellorsUK\Providers;

use CounsellorsUK\Events\ProfileSaving;
use CounsellorsUK\Events\UserCreated;
use CounsellorsUK\Models\Profile;
use CounsellorsUK\Models\User;
use Event;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'CounsellorsUK\Events\UserCreated' => [
            'CounsellorsUK\Listeners\PrepareProfile',
        ],
        'CounsellorsUK\Events\ProfileSaving' => [
            'CounsellorsUK\Listeners\ProcessVerifyLive',
        ],
        'CounsellorsUK\Events\UserSubscribed' => [
            'CounsellorsUK\Listeners\ProcessSubscription',
        ],
    ];

    /**
     * Register any other events for your application.
     */
    public function boot()
    {
        parent::boot();

        // Pass through Eloquent events
        User::created(function ($user) {
            Event::fire(new UserCreated($user));
        });

        Profile::saving(function ($profile) {
            Event::fire(new ProfileSaving($profile));
        });
    }
}
