<?php

namespace CounsellorsUK\Http\Requests;

class ProfileDetailsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $string_max = config('database.column_max.string');
        $text_max = config('database.column_max.text');

        return [
            'registration_number' => "max:{$string_max}",
            'bacp_certificate_number' => "max:{$string_max}",
            'main_qualification' => "required|max:{$string_max}",
            'is_insured' => 'accepted',

            'body_memberships' => 'required|array',
            'body_memberships.*' => 'integer',

            'about' => "required|max:{$text_max}",

            'counselling_hours' => 'enforce_json_structure:total,years',
            'counselling_hours.*' => 'numeric',
        ];
    }
}
