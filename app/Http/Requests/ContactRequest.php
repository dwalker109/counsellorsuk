<?php

namespace CounsellorsUK\Http\Requests;

class ContactRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $string_max = config('database.column_max.string');
        $text_max = config('database.column_max.text');

        return [
            'name' => "max:{$string_max}",
            'email' => 'required_if:contact_preference,email|email',
            'phone' => 'required_if:contact_preference,phone|phone:GB',
            'contact_preference' => 'required',
            'message' => "required|max:{$text_max}",
            'g-recaptcha-response' => 'recaptcha',
        ];
    }
}
