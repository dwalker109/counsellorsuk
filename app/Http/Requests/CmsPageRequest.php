<?php

namespace CounsellorsUK\Http\Requests;

use CounsellorsUK\Models\CmsPage;

class CmsPageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $string_max = config('database.column_max.string');
        $mediumtext_max = config('database.column_max.mediumtext');
        $permissions = collect(CmsPage::$permissions)->keys()->implode(',');
        
        return [
            'parent_id' => 'integer',
            'title' => "required|max:{$string_max}",
            'body' => "required|max:{$mediumtext_max}",
            'meta_description' => "required|max:{$string_max}",
            'permission' => "required|in:{$permissions}",
        ];
    }
}
