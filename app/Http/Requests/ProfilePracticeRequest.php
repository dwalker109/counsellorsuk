<?php

namespace CounsellorsUK\Http\Requests;

class ProfilePracticeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $string_max = config('database.column_max.string');
        $text_max = config('database.column_max.text');

        return [
            'session_lengths' => 'enforce_json_structure:initial,subsequent',
            'session_lengths.*' => 'numeric',

            'appointments_at' => 'enforce_json_structure:weekdays,some_evenings,weekends',
            'appointments_at.*' => 'boolean',

            'fees' => 'enforce_json_structure:adults,adolescents,families,couples,children,concessions',
            'fees.*' => 'numeric',

            'probono_available' => 'boolean',
            'probono_about' => "required_if:probono_available,1|max:{$string_max}",

        ];
    }
}
