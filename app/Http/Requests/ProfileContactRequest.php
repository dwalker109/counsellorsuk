<?php

namespace CounsellorsUK\Http\Requests;

use Auth;

class ProfileContactRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();
        $string_max = config('database.column_max.string');

        return [
            'name' => "required|max:{$string_max}",
            'email' => "required|email|max:{$string_max}|unique:users,email,{$user->id}",
            
            'phone_numbers' => 'enforce_json_structure:primary,additional',
            'phone_numbers.*' => 'phone:GB',

            'urls' => 'enforce_json_structure:primary,additional',
            'urls.*' => "url|max:{$string_max}",

            'social_media' => 'enforce_json_structure:facebook,twitter,linkedin',
            'social_media.*' => "url|max:{$string_max}",
        ];
    }
}
