<?php

namespace CounsellorsUK\Http\Requests;

class BodyMembershipRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $string_max = config('database.column_max.string');
        
        return [
            'body' => "required|max:{$string_max}",
            'name' => "required|max:{$string_max}",
            'is_supervisory' => 'boolean',
        ];
    }
}
