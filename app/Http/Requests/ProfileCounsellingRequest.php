<?php

namespace CounsellorsUK\Http\Requests;

class ProfileCounsellingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'approaches' => 'array',
            'approaches.*' => 'integer',

            'issues' => 'array',
            'issues.*' => 'integer',

            'other_services' => 'array',
            'other_services.*' => 'integer',

            'skills' => 'array',
            'skills.*' => 'integer',
        ];
    }
}
