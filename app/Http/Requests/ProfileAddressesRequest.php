<?php

namespace CounsellorsUK\Http\Requests;

class ProfileAddressesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $string_max = config('database.column_max.string');
        $text_max = config('database.column_max.text');

        return [
            'addresses.*.name' => "max:{$string_max}",
            'addresses.*.address_1' => "required_if:addresses.*.add,1|max:{$string_max}",
            'addresses.*.address_2' => "max:{$string_max}",
            'addresses.*.location_id' => 'required_if:addresses.*.add,1|integer',
            'addresses.*.postal_code' => 'required_if:addresses.*.add,1|max:10',
            'addresses.*.show_full_address' => 'required_if:addresses.*.add,1|boolean',
        ];
    }
}
