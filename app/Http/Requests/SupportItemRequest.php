<?php

namespace CounsellorsUK\Http\Requests;

class SupportItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $string_max = config('database.column_max.string');
        $text_max = config('database.column_max.text');

        return [
            'name' => "required|max:{$string_max}",
            'icon' => 'max:128',
        ];
    }
}
