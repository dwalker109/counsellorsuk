<?php

namespace CounsellorsUK\Http\Requests;

class SelectedMemberRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $string_max = config('database.column_max.string');

        return [
            'name' => "required|max:{$string_max}",
            'email' => "required|email|max:{$string_max}|unique:users",
        ];
    }
}
