<?php

namespace CounsellorsUK\Http\Controllers\Members;

use Auth;
use CounsellorsUK\Helpers\BraintreeHelper;
use CounsellorsUK\Http\Controllers\Controller;
use Event;
use Exception;
use Illuminate\Http\Request;
use Laravel\Cashier\BraintreeService;
use Notification;
use CounsellorsUK\Events\UserSubscribed;

class SubscriptionController extends Controller
{
    /**
     * Create a new subscription controller instance.
     */
    public function __construct()
    {
        $this->subscription_name = config('services.braintree.subscription_name');
    }

    /**
     * Show the subscription purchase or update details page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plan = BraintreeService::findPlan(config('services.braintree.subscription_plan_id'));
        $user = Auth::user();
        $subscription_name = $this->subscription_name;

        $view = $user->subscribed() ? 'members.update-subscription' : 'members.subscribe';

        return view($view, compact('plan', 'user', 'subscription_name'));
    }

    /**
     * Add a new subscription.
     *
     * @return \Illuminate\Http\Response
     */
    public function newSubscription(Request $request)
    {
        try {
            Auth::user()
            ->newSubscription($this->subscription_name, $request->plan)
            ->withCoupon($request->coupon ?? null)
            ->create($request->payment_method_nonce);
        } catch (Exception $e) {
            Notification::error(view('members._braintree_error')->render());
            return redirect()->back();
        }

        // Fire an event to carry out any additional processing
        Event::fire(new UserSubscribed(Auth::user()));

        // Supress subscription nag, set notification message
        $request->session()->flash('just_subscribed', true);
        Notification::success(view('members._braintree_success')->render());

        return redirect()->route('profile.view');
    }

    /**
     * Resume the subscription.
     *
     * @return \Illuminate\Http\Response
     */
    public function resume()
    {
        Auth::user()
            ->subscription($this->subscription_name)
            ->resume();

        return back();
    }

    /**
     * Update the stored card token
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function updateCard(Request $request)
    {
        Auth::user()
            ->updateCard($request->payment_method_nonce);

        Notification::success('Your payment details have been updated.');
        return redirect()->route('profile.view');
    }
}
