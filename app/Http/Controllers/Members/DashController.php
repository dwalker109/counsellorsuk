<?php

namespace CounsellorsUK\Http\Controllers\Members;

use CounsellorsUK\Http\Controllers\Controller;

class DashController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('members.home');
    }
}
