<?php

namespace CounsellorsUK\Http\Controllers\Members;

use Agent;
use Auth;
use Carbon\Carbon;
use CounsellorsUK\Helpers\StatisticsHelper;
use CounsellorsUK\Http\Controllers\Controller;
use CounsellorsUK\Http\Requests\PasswordRequest;
use CounsellorsUK\Http\Requests\ProfileAddressesRequest;
use CounsellorsUK\Http\Requests\ProfileContactRequest;
use CounsellorsUK\Http\Requests\ProfileCounsellingRequest;
use CounsellorsUK\Http\Requests\ProfileDeleteRequest;
use CounsellorsUK\Http\Requests\ProfileDetailsRequest;
use CounsellorsUK\Http\Requests\ProfilePhotoRequest;
use CounsellorsUK\Http\Requests\ProfilePracticeRequest;
use CounsellorsUK\Mail\VerifyAddressMail;
use CounsellorsUK\Models\Address;
use CounsellorsUK\Models\Approach;
use CounsellorsUK\Models\BodyMembership;
use CounsellorsUK\Models\CmsPage;
use CounsellorsUK\Models\Issue;
use CounsellorsUK\Models\Location;
use CounsellorsUK\Models\OtherService;
use CounsellorsUK\Models\Profile;
use CounsellorsUK\Models\Skill;
use CounsellorsUK\Models\Statistic;
use CounsellorsUK\Models\User;
use Hash;
use Illuminate\Http\Request;
use Mail;
use Notification;

class ProfileController extends Controller
{
    /**
     * Show the profile overview page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = Auth::user()->profile;

        return view('members.profile', compact('profile'));
    }

    /**
     * Send an address verification email.
     *
     * @return \Illuminate\Illuminate\Http\Response
     */
    public function sendVerifyEmail()
    {
        $user = Auth::user();

        $payload = collect([
            'hash' => Hash::make($user->id),
            'expiry' => Carbon::now()->addMonth(),
        ]);

        $user->email_verify_token = encrypt($payload);
        $user->email_verified = false;
        $user->save();

        Mail::to($user)->queue(new VerifyAddressMail($user));

        Notification::success("An email has been sent to {$user->email}. "
            . 'Please follow the instruction in the email to verify your address.');

        return redirect()->back();
    }

    /**
     * Process an email verification request.
     *
     * @param string
     * @return \Illuminate\Illuminate\Http\Response
     */
    public function processVerifyEmail($email_verify_token)
    {
        // Wrap all lookups and checking in try/catch to handle malformed
        // and malicious attempts - each check will throw an exception on failure
        try {
            $payload = decrypt($email_verify_token);

            $user = User::where('email_verify_token', $email_verify_token)->firstOrFail();

            if (! Hash::check($user->id, $payload->get('hash'))) {
                throw new \Exception('Hash did not match');
            }

            if ($payload->get('expiry')->lt(Carbon::now())) {
                throw new \Exception('Token has expired');
            }
        } catch (\Exception $e) {
            abort(404);
        }

        $user->email_verified = true;
        $user->email_verify_token = null;
        $user->save();

        Auth::login($user, true);

        Notification::success('Your email address has been verified - thanks!');

        return redirect()->route('profile.view');
    }

    /**
     * Show the Counsellors Area base page
     *
     * @return \Illuminate\Illuminate\Http\Response
     */
    public function memberBenefits()
    {
        $page = CmsPage::where('menu_handle', 'member-benefits')->firstOrFail();

        $children = $page->children()->defaultOrder()->get()->filter(function ($page) {
            // Filter subscriber only pages out if member is not a subscriber
            return $page->applied_permission == CmsPage::SUBSCRIBER && !Auth::user()->profile->meta_is_subscriber
                ? false
                : true;
        });

        return view('members.member_benefits', compact('page', 'children'));
    }

    /**
     * Show the password update form
     *
     * @return \Illuminate\Http\Response
     */
    public function viewPassword()
    {
        $user = Auth::user();

        return view('members.password', compact('user'));
    }

    /**
     * Process the update password form.
     *
     * @param \CounsellorsUK\Http\Requests\PasswordRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(PasswordRequest $request)
    {
        $user = Auth::user();

        $user->password = Hash::make($request->get('new_password'));
        $user->save();

        Notification::success('Your password has been updated.');

        return redirect()->route('profile.view');
    }

    /**
     * Show the photo update form.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewPhoto()
    {
        $profile = Auth::user()->profile;

        return view('members.profile_photo', compact('profile'));
    }

    /**
     * Process the photo update form.
     *
     * @param \CounsellorsUK\Http\Requests\ProfilePhotoRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function updatePhoto(ProfilePhotoRequest $request)
    {
        $profile = Auth::user()->profile;

        $profile->setProfileImage($request);

        $profile->markStageComplete(Profile::PHOTO);

        Notification::success($profile->stages->all->get(Profile::PHOTO)->description
            .' has been updated.');

        return redirect()->route('profile.view');
    }

    /**
     * Process the photo remove form.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function deletePhoto(Request $request)
    {
        $profile = Auth::user()->profile;

        $profile->deleteProfileImage();

        $profile->markStageIncomplete(Profile::PHOTO);

        Notification::success($profile->stages->all->get(Profile::PHOTO)->description
            .' has been removed.');

        return redirect()->route('profile.view');
    }

    /**
     * Show the basic details form.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewDetails()
    {
        $profile = Auth::user()->profile;

        // Body memberships, grouped by body
        foreach (BodyMembership::all()->groupBy('body') as $body => $membership) {
            $body_memberships[$body] = $membership->pluck('name', 'id')->toArray();
        }

        return view('members.profile_details', compact('profile', 'body_memberships'));
    }

    /**
     * Process the basic details form.
     *
     * @param \CounsellorsUK\Http\Requests\ProfileDetailsRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function updateDetails(ProfileDetailsRequest $request)
    {
        $profile = Auth::user()->profile;

        $profile->fill($request->all())->save();
        $profile->body_memberships()->sync($request->body_memberships ?? []);

        $profile->markStageComplete(Profile::DETAILS);

        Notification::success($profile->stages->all->get(Profile::DETAILS)->description
            .' have been updated.');

        return redirect()->route('profile.view');
    }

    /**
     * Show the contact details form.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewContact()
    {
        $profile = Auth::user()->profile;

        return view('members.profile_contact', compact('profile'));
    }

    /**
     * Process the contact details form.
     *
     * @param \CounsellorsUK\Http\Requests\ProfileContactRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function updateContact(ProfileContactRequest $request)
    {
        $profile = Auth::user()->profile;
        $user = $profile->user;

        $profile->fill($request->except(['name', 'email']))->save();
        $user->fill($request->only(['name', 'email']));

        if ($user->isDirty('email')) {
            $user->email_verified = false;
            $user->email_verify_token = null;
        }

        $user->save();

        $profile->markStageComplete(Profile::CONTACT);

        Notification::success($profile->stages->all->get(Profile::CONTACT)->description
            .' have been updated.');

        return redirect()->route('profile.view');
    }

    /**
     * Show a practice address form.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewAddresses()
    {
        $profile = Auth::user()->profile()->with(['addresses.location'])->first();

        $locations = $profile->addresses->map(function ($address) {
            return $address->location;
        })->pluck('name', 'id');

        return view('members.profile_addresses', compact('profile', 'locations'));
    }

    /**
     * Process the practice address form.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateAddresses(ProfileAddressesRequest $request)
    {
        $profile = Auth::user()->profile;
        $addresses = $request->addresses;

        // Set the first practice address posted as primary
        array_set($addresses, '0.is_primary', true);

        // Remove 'additional' array if not enabled
        if (!array_get($addresses, '1.add')) {
            unset($addresses[1]);
        }

        // Destroy any existing related practice_addresses
        if (!$profile->addresses->isEmpty()) {
            Address::destroy($profile->addresses()->pluck('id')->toArray());
        }

        // Save each newly posted practice
        foreach ($addresses as $address) {
            $profile->addresses()->create($address);
        }

        $profile->markStageComplete(Profile::ADDRESSES);

        Notification::success($profile->stages->all->get(Profile::ADDRESSES)->description
            .' have been updated.');

        return redirect()->route('profile.view');
    }

    /**
     * Show the practice details form.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewPractice()
    {
        $profile = Auth::user()->profile;

        return view('members.profile_practice', compact('profile'));
    }

    /**
     * Process the practice details form.
     *
     * @return \Illuminate\Http\Response
     */
    public function updatePractice(ProfilePracticeRequest $request)
    {
        $profile = Auth::user()->profile;

        $profile->fill($request->all())->save();

        $profile->markStageComplete(Profile::PRACTICE);

        Notification::success($profile->stages->all->get(Profile::PRACTICE)->description
            .' have been updated.');

        return redirect()->route('profile.view');
    }

    /**
     * Show the counselling details form.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewCounselling()
    {
        $profile = Auth::user()->profile;

        $approaches = Approach::getOptionsForSelect2();
        $issues = Issue::getOptionsForSelect2();
        $other_services = OtherService::getOptionsForSelect2();
        $skills = Skill::getOptionsForSelect2();

        return view(
            'members.profile_counselling',
            compact(
                'profile',
                'approaches',
                'issues',
                'other_services',
                'skills'
            )
        );
    }

    /**
     * Process the counselling details form.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateCounselling(ProfileCounsellingRequest $request)
    {
        $profile = Auth::user()->profile;

        $profile->approaches()->sync($request->approaches ?? []);
        $profile->issues()->sync($request->issues ?? []);
        $profile->other_services()->sync($request->other_services ?? []);
        $profile->skills()->sync($request->skills ?? []);

        $profile->markStageComplete(Profile::COUNSELLING);

        Notification::success($profile->stages->all->get(Profile::COUNSELLING)->name
            .' details have been updated.');

        return redirect()->route('profile.view');
    }

    /**
     * Show the delete profile form.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewDelete()
    {
        return view('members.profile_delete');
    }

    /**
     * Delete the delete profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function doDelete(ProfileDeleteRequest $request)
    {
        $user = Auth::user();

        StatisticsHelper::record($user->profile, StatisticsHelper::PROFILE_DELETED);
        
        Auth::logout();

        $user->profile->deleteProfileImage();
        $user->delete();
        
        Notification::success('Your profile has been deleted. Please feel free to create a new account if you change your mind!');

        return redirect()->route('home');
    }
}
