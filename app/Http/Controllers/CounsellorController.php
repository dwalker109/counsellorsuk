<?php

namespace CounsellorsUK\Http\Controllers;

use Cache;
use Carbon\Carbon;
use CounsellorsUK\Http\Requests;
use CounsellorsUK\Http\Requests\ClaimProfileRequest;
use CounsellorsUK\Http\Requests\ContactRequest;
use CounsellorsUK\Mail\ContactCounsellorMail;
use CounsellorsUK\Mail\MagicLinkMail;
use CounsellorsUK\Models\Profile;
use Exception;
use Illuminate\Http\Request;
use Mail;
use Mapper;
use Notification;
use Statistics;
use Uuid;

class CounsellorController extends Controller
{
    /**
     * Show a counsellor's profile.
     *
     * @param \CounsellorsUK\Models\Profile $profile
     *
     * @return \Illuminate\Http\Response
     */
    public function view(Profile $profile)
    {
        // Get required maps - catch lookup exceptions and add a found/not found
        // variable to inform the view whether the map should be used or not,
        // and add a placeholder map which will prevent a key mismatch later.
        $address_located = [];
        foreach ($profile->addresses as $key => $address) {
            try {
                $location = Cache::rememberForever(
                    'location.' . sha1($address->present()->address_lookup),
                    function () use ($address) {
                        return Mapper::location($address->present()->address_lookup);
                    }
                );

                Mapper::map($location->getLatitude(), $location->getLongitude());
                $address_located[$key] = true;
            } catch (Exception $e) {
                Mapper::map(0, 0);
                $address_located[$key] = false;
                \Log::debug($e->getMessage());
            }
        }

        $view = $profile->meta_is_subscriber ? 'premium_profile' : 'free_profile';

        return view("counsellor.{$view}", compact('profile', 'address_located'));
    }
    
    /**
     * Show a counsellor contact form.
     *
     * @param \CounsellorsUK\Models\Profile $profile
     *
     * @return \Illuminate\Http\Response
     */
    public function contact(Profile $profile)
    {
        if (!$this->canReceiveEmail($profile)) {
            return redirect()->route('counsellor', $profile);
        }
        
        return view('counsellor.contact', compact('profile'));
    }
    
    /**
     * Process a counsellor contact form.
     *
     * @param \CounsellorsUK\Http\Requests\ContactRequest
     * @param \CounsellorsUK\Models\Profile $profile
     *
     * @return \Illuminate\Http\Response
     */
    public function contactSend(ContactRequest $request, Profile $profile)
    {
        if (!$this->canReceiveEmail($profile)) {
            return redirect()->route('counsellor', $profile);
        }

        $info = $request->except('_token', 'g-recaptcha-response');

        // Queue initial referral mail
        Mail::to($profile->user)->queue(new ContactCounsellorMail($info));

        // Queue delayed followup mail
        Mail::to($profile->user)->later(
            config('counsellorsuk.contact_followup_delay'),
            new ContactCounsellorMail($info, $is_followup = true)
        );

        Statistics::record($profile, Statistics::PROFILE_CONTACTED);

        Notification::success('Your message has been sent - you should receive a reply promptly.');
        
        return redirect()->route('counsellor', $profile);
    }
    
    /**
     * Convenience method to check if emailing is authorised for this profile.
     *
     * @param \CounsellorsUK\Models\Profile $profile
     *
     * @return boolean
     */
    private function canReceiveEmail(Profile $profile)
    {
        if (config('counsellorsuk.allow_contact_to_unverified_email')) {
            return true;
        }
        
        if (!$profile->user->email_verified) {
            Notification::error(
                'This counsellor has not yet verified their email address. '
                . 'Please use an alternative method of contact.'
            );
            
            return false;
        }
        
        return true;
    }


    public function claimProfile(Profile $profile)
    {
        return view('counsellor.claim_profile', compact('profile'));
    }

    /**
     * Claim a profile by sending a magic link to the associated user's email
     *
     * @param ClaimProfileRequest $request
     * @param Profile $profile
     *
     * @return void
     */
    public function claimProfileSend(ClaimProfileRequest $request, Profile $profile)
    {
        $user = $profile->user;

        $user->refreshMagicLink();

        Cache::put(
            $short_link = Uuid::generate()->string,
            $user->magic_link_token,
            decrypt($user->magic_link_token)->get('expiry')
        );

        Mail::to($user)->queue(new MagicLinkMail($user, $short_link));

        Notification::success('To login in and claim your profile, please check your email.');

        return redirect()->route('counsellor', compact('profile'));
    }
}
