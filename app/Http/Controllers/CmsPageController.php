<?php

namespace CounsellorsUK\Http\Controllers;

use Agent;
use Auth;
use Cache;
use CounsellorsUK\Http\Requests;
use CounsellorsUK\Http\Requests\ContactRequest;
use CounsellorsUK\Mail\ContactUsMail;
use CounsellorsUK\Models\CmsPage;
use CounsellorsUK\Models\Profile;
use Illuminate\Http\Request;
use Mail;
use Notification;

class CmsPageController extends Controller
{
    /**
     * Show the homepage.
     *
     * @return Illuminate\Http\Response
     */
    public function home()
    {
        $member_count = Cache::remember('member_count', 60, function () {
            // Round count down to nearest thousand
            $count = Profile::count();
            return $count - ($count % 1000);
        });

        $permalinks = Cache::remember('homepage_permalinks', 60, function () {
            return collect([
                CmsPage::where('menu_handle', 'cic')->firstOrFail(),
                CmsPage::where('menu_handle', 'bodies')->firstOrFail(),
            ])->keyBy('menu_handle');
        });
        
        return view('home', compact('member_count', 'permalinks'));
    }

    /**
     * Show a CMS content page, after checking user permission.
     *
     * @param CounsellorsUK\Models\CmsPage $cms_page
     * @return Illuminate\Http\Response
     */
    public function cms(CmsPage $cms_page)
    {
        $user = Auth::user();

        switch ($cms_page->applied_permission) {
            // User must be a paying subscriber
            case CmsPage::SUBSCRIBER:
                if (!$user || !$user->profile->meta_is_subscriber) {
                    abort(403);
                }
                break;

            // User must be logged in
            case CmsPage::MEMBER:
                if (!$user) {
                    abort(403);
                }
                break;

            // No restriction - do nothing
            default:
        }

        return view(
            request()->ajax() ? 'cms_page.ajax' : 'cms_page.default',
            compact('cms_page')
        );
    }

    /**
     * Show the contact page.
     *
     * @return Illuminate\Http\Response
     */
    public function contact()
    {
        return view('contact');
    }

    /**
     * Process the contact form.
     *
     * @param \CounsellorsUK\Http\Requests\ContactRequest
     * @return \Illuminate\Http\Response
     */
    public function contactSend(ContactRequest $request)
    {
        $info = $request->except('_token', 'g-recaptcha-response');

        Mail::to(config('counsellorsuk.notification_email.address'))
            ->queue(new ContactUsMail($info));

        Notification::success('Your message has been sent - you will receive a reply within 48 hours.');

        return redirect()->route('home');
    }
}
