<?php

namespace CounsellorsUK\Http\Controllers;

use App;
use Carbon\Carbon;
use CounsellorsUK\Http\Requests;
use CounsellorsUK\Models\CmsPage;
use CounsellorsUK\Models\Location;
use CounsellorsUK\Models\Profile;
use Illuminate\Http\Request;

class SitemapController extends Controller
{
    public function index()
    {
        $sitemap = App::make('sitemap');
        $sitemap->setCache('laravel.sitemap', 60, $cached = true);

        if (!$sitemap->isCached()) {
            // Manually add static routes
            foreach (['home', 'contact'] as $route) {
                $sitemap->add(
                    route($route),
                    Carbon::now(),
                    0.7,
                    'monthly'
                );
            }

            // Manually add certain CMS pages
            foreach (['in-crisis', 'frequently-asked-questions'] as $slug) {
                $page = CmsPage::where('slug', $slug)->first();
                
                if (!$page) {
                    continue;
                }
                
                $sitemap->add(
                    route('cms_page', $page->slug),
                    $page->updated_at,
                    0.7,
                    'monthly'
                );
            }
            
            // Manually add menu handle sub items
            foreach (['about', 'therapies', 'issues'] as $menu_handle) {
                $pages = CmsPage::where('menu_handle', $menu_handle)->first();

                foreach ($pages->children ?? [] as $page) {
                    $sitemap->add(
                        route('cms_page', $page->slug),
                        $page->updated_at,
                        0.7,
                        'monthly'
                    );
                }
            }

            // Add all counsellor profiles
            foreach (Profile::all() as $profile) {
                $sitemap->add(
                    route('counsellor', $profile),
                    $profile->updated_at,
                    0.9,
                    'monthly'
                );
            }
            
            // Add all locations
            foreach (Location::towns()->get() as $town) {
                $sitemap->add(
                    route('search.results', ['location' => $town->slug]),
                    Carbon::now(),
                    0.8,
                    'daily'
                );
            }
        }

        return $sitemap->render('xml');
    }
}
