<?php

namespace CounsellorsUK\Http\Controllers;

use CounsellorsUK\Http\Requests\SearchRequest;
use CounsellorsUK\Models\Issue;
use CounsellorsUK\Models\Location;
use CounsellorsUK\Models\Profile;
use Illuminate\Http\Request;
use Session;

class SearchController extends Controller
{
    /**
     * Build the member index of locations - an alternate way of searching.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $counties = Location::counties()->with(['children' => function ($query) {
            $query->orderBy('name');
        }])->get();

        return view('search.index', compact('counties'));
    }

    /**
     * Redirect posted search form to a pretty search URL.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function prepare(SearchRequest $request)
    {
        $location = $request->get('location', key(config('counsellorsuk.search.unfiltered.location')));
        $distance = $request->get('distance', key(config('counsellorsuk.search.unfiltered.distance')));
        $issue = $request->get('issue', key(config('counsellorsuk.search.unfiltered.issue')));

        return redirect()->route('search.results', [$location, $distance, $issue]);
    }

    /**
     * Carry out a search.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $location
     * @param string                   $distance
     * @param string                   $issue
     *
     * @return \Illuminate\Http\Response
     */
    public function results(Request $request, $location, $distance = null, $issue = null)
    {
        $matched = (object) [
            'location' => Location::where('slug', $location)->firstOrFail(),
            'distance' => config("counsellorsuk.search.distances.{$distance}", null),
            'issue' => Issue::where('slug', $issue)->first(),
        ];

        $results = Profile::query()
            ->with('user', 'body_memberships', 'issues', 'other_services', 'skills')
            ->with(['addresses.location' => function ($query) use ($matched) {
                $query->addDistanceColumn($matched->location);
            }])
            ->orderBy('profiles.meta_is_subscriber', 'desc')
            ->filterLocation($matched->location, $matched->distance)
            ->filterIssue($matched->issue)
            ->distinct()
            ->groupBy('profiles.id')
            ->orderBy('profiles.rotating_sort')
            ->orderBy('profiles.slug')
            ->paginate(10);

        // If nothing was found, expand the search radius (if possible)
        if ($results->isEmpty() && $matched->distance < last(config('counsellorsuk.search.distances'))) {
            $next_distance = key(
                array_slice(
                    config('counsellorsuk.search.distances'),
                    $offset = array_search($distance, array_keys(config('counsellorsuk.search.distances'))) + 1,
                    $size = 1,
                    $preserveKeys = true
                )
            );

            $request->session()->flash('search_was_expanded', true);

            return redirect()->route('search.results', [$location, $next_distance, $issue]);
        }

        // Keep the URL of this search for use in nav links
        $request->session()->put(
            'last_search_url',
            $request->getPathInfo() . ($request->getQueryString() ? ('?'.$request->getQueryString()) : '')
        );

        // Note use of make()->with([...]) here - defaults are set in a view creator, which with() overrides
        return view()->make('search.results')->with([
            'location' => $matched->location
                ? [$matched->location->slug => $matched->location->name]
                : config('counsellorsuk.search.unfiltered.location'),
            'distance' => $matched->distance
                ? $distance
                : key(config('counsellorsuk.search.unfiltered.distance')),
            'issue' => $matched->issue
                ? [$matched->issue->slug => $matched->issue->name]
                : config('counsellorsuk.search.unfiltered.issue'),
            'results' => $results,
        ]);
    }
}
