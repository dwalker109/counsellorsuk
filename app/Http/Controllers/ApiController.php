<?php

namespace CounsellorsUK\Http\Controllers;

use CounsellorsUK\Models\Approach;
use CounsellorsUK\Models\Issue;
use CounsellorsUK\Models\Location;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * Look up locations (exact search, no wildcarding).
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function lookupLocation(Request $request)
    {
        $term = sprintf('"%s"', $request->get('term'));
        return Location::search($term)->orderBy('name')->paginate(15);
    }

    /**
     * Look up towns/boroughs (exact search, no wildcarding).
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function lookupTown(Request $request)
    {
        $term = sprintf('"%s"', $request->get('term'));
        return Location::search($term)->towns()->orderBy('name')->paginate(15);
    }

    /**
     * Look up issues.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function lookupIssue(Request $request)
    {
        return Issue::search($request->get('term'))->orderBy('name')->paginate(15);
    }

    /**
     * Look up approaches.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function lookupApproach(Request $request)
    {
        return Approach::search($request->get('term'))->orderBy('name')->paginate(15);
    }
}
