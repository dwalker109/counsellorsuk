<?php

namespace CounsellorsUK\Http\Controllers\Admin;

use CounsellorsUK\Models\OtherService;

class OtherServiceController extends SupportItemsController
{
    /**
     * Initialise a new other service controller instance.
     */
    public function __construct(OtherService $other_service)
    {
        $this->class = $other_service;
        parent::__construct();
    }
}
