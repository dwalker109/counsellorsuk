<?php

namespace CounsellorsUK\Http\Controllers\Admin;

use CounsellorsUK\Models\Skill;

class SkillController extends SupportItemsController
{
    /**
     * Initialise a new skill controller instance.
     */
    public function __construct(Skill $skill)
    {
        $this->class = $skill;
        parent::__construct();
    }
}
