<?php

namespace CounsellorsUK\Http\Controllers\Admin;

use Illuminate\Http\Request;

use CounsellorsUK\Http\Requests;
use CounsellorsUK\Http\Controllers\Controller;
use CounsellorsUK\Http\Requests\BodyMembershipRequest;
use CounsellorsUK\Models\BodyMembership;
use Excel;

class BodyMembershipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $body_memberships = BodyMembership::orderBy('body')->orderBy('name')->get();
        
        return view('admin.body_memberships.index', compact('body_memberships'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.body_memberships.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \CounsellorsUK\Http\Requests\BodyMembershipRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BodyMembershipRequest $request)
    {
        BodyMembership::create($request->all());

        return redirect()->route('admin.body_membership.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \CounsellorsUK\Models\BodyMembership  $body_membership
     * @return \Illuminate\Http\Response
     */
    public function show(BodyMembership $body_membership)
    {
        return abort(501, 'Not implemented');
    }
    
    /**
     * Export summary of the specified resource membership.
     *
     * @param  \CounsellorsUK\Models\BodyMembership  $body_membership
     * @return \Illuminate\Http\Response
     */
    public function export(BodyMembership $body_membership)
    {
        Excel::create("{$body_membership->name}_export", function ($excel) use ($body_membership) {
            $excel->sheet('Members', function ($sheet) use ($body_membership) {
                $sheet->fromArray(
                    $body_membership->profiles->transform(function ($profile) {
                        return [
                            'Name' => $profile->user->name,
                            'Email' => $profile->user->email,
                            'Registration Number' => $profile->registration_number,
                        ];
                    })->toArray()
                );
            });
        })->download('xlsx');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \CounsellorsUK\Models\BodyMembership  $body_membership
     * @return \Illuminate\Http\Response
     */
    public function edit(BodyMembership $body_membership)
    {
        return view('admin.body_memberships.edit', compact('body_membership'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \CounsellorsUK\Http\Requests\BodyMembershipRequest  $request
     * @param  \CounsellorsUK\Models\BodyMembership  $body_membership
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BodyMembership $body_membership)
    {
        $body_membership->fill($request->all())->save();
        
        return redirect()->route('admin.body_membership.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \CounsellorsUK\Models\BodyMembership  $body_membership
     * @return \Illuminate\Http\Response
     */
    public function destroy(BodyMembership $body_membership)
    {
        $body_membership->delete();
        
        return redirect()->route('admin.body_membership.index');
    }
}
