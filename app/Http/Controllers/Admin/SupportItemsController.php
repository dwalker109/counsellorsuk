<?php

namespace CounsellorsUK\Http\Controllers\Admin;

use ArrayObject;
use CounsellorsUK\Http\Controllers\Controller;
use CounsellorsUK\Http\Requests\SupportItemRequest;

abstract class SupportItemsController extends Controller
{
    /**
     * The display name of this item, in various forms.
     *
     * @var ArrayObject
     */
    protected $display_name;

    /**
     * An instance of the support item class.
     *
     * @var string
     */
    protected $class;

    /**
     * Create a new support items controller instance.
     */
    public function __construct()
    {
        $this->display_name = $this->getDisplayName();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = $this->class->all()->sortBy('name');
        $display_name = $this->display_name;

        return view('admin.support_items.index', compact('items', 'display_name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $display_name = $this->display_name;

        return view('admin.support_items.create', compact('display_name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SupportItemRequest $request)
    {
        $item = $this->class->create($request->all());
        $item->setIcon($request);

        return redirect()->route("admin.{$this->display_name->snake_case}.index");
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(501, 'Not implemented');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = $this->class->findOrFail($id);

        $display_name = $this->display_name;

        return view('admin.support_items.edit', compact('item', 'display_name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SupportItemRequest $request, $id)
    {
        $item = $this->class->findOrFail($id);
        $item->fill($request->all())->save();
        $item->setIcon($request);

        return redirect()->route("admin.{$this->display_name->snake_case}.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->class->findOrFail($id)->delete();

        return redirect()->route("admin.{$this->display_name->snake_case}.index");
    }

    /**
     * Generate the different display name forms for the support item.
     *
     * @return ArrayObject;
     */
    private function getDisplayName()
    {
        $class_basename = class_basename(get_class($this->class));
        $forms = new ArrayObject([
            'snake_case' => snake_case($class_basename),
            'singular' => $class_basename,
            'plural' => str_plural($class_basename),
        ]);
        $forms->setFlags(ArrayObject::ARRAY_AS_PROPS);

        return $forms;
    }
}
