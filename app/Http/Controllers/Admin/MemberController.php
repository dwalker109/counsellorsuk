<?php

namespace CounsellorsUK\Http\Controllers\Admin;

use Artisan;
use Auth;
use CounsellorsUK\Http\Controllers\Controller;
use CounsellorsUK\Http\Requests\SelectedMemberRequest;
use CounsellorsUK\Models\User;
use Illuminate\Http\Request;
use Notification;

class MemberController extends Controller
{
    /**
     * Display a listing of members.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sort_options = collect(config('counsellorsuk.search.admin_member_sort_options'));
        
        $selected_sort = (object) $sort_options->get(
            $request->get('sort', null),
            $sort_options->first()
        );

        $users = User::search($request->get('search'))
            ->orderBy($selected_sort->column, $selected_sort->order)
            ->paginate(10)
            ->appends($request->all());

        return view('admin.members.index', compact('users', 'sort_options'));
    }

    /**
     * Login (impersonate) a member.
     *
     * @param User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function impersonate(User $user)
    {
        Auth::login($user);

        return redirect()->route('dashboard');
    }

    /**
     * Toggle a manual subscription status.
     *
     * @param User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function toggleLegacySubscriber(User $user)
    {
        if ($user->subscribed()) {
            Notification::error("$user->name already has a subscription");

            return back();
        }

        $user->subscription_override = $user->subscription_override ? false : true;
        $user->save();

        // Queue an update of profile meta columns
        Artisan::queue('profiles:rebuild-meta');

        return back();
    }

    /**
     * Toggle a verified status.
     *
     * @param User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function toggleVerified(User $user)
    {
        $user->profile->is_verified = ! $user->profile->is_verified;
        $user->profile->save();

        return back();
    }

    /**
     * Toggle a live status.
     *
     * @param User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function toggleLive(User $user)
    {
        $user->profile->is_live = ! $user->profile->is_live;
        $user->profile->save();

        return back();
    }

    /**
     * Show a form allowing a selected counsellor to be added to the site
     * on their behalf.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.members.create');
    }

    /**
     * Save a newly created member and auto login.
     *
     * @param \CounsellorsUK\Http\Requests\Request\SelectedMemberRequest
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SelectedMemberRequest $request)
    {
        $user = new User;
        $user->fill($request->all());
        $user->source = User::SOURCE_SELECTED;
        $user->save();

        return $this->impersonate($user);
    }
    
    /**
     * Delete a user.
     *
     * @param User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->profile->meta_is_subscriber) {
            Notification::error("{$user->name} has a subscription. You must deactivate "
                .' or cancel the subscription before you can delete this member.');
            
            return back();
        }
        
        $user->profile->deleteProfileImage();
        $user->delete();
        
        return back();
    }
}
