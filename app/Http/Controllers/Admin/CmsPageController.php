<?php

namespace CounsellorsUK\Http\Controllers\Admin;

use CounsellorsUK\Http\Controllers\Controller;
use CounsellorsUK\Http\Requests\CmsPageRequest;
use CounsellorsUK\Models\CmsPage;
use LogicException;
use Notification;

class CmsPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cms_pages = CmsPage::defaultOrder()->get()->toTree();

        return view('admin.cms_pages.index', compact('cms_pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parent_options = CmsPage::getOptionsForSelect2();
        $permission_options = CmsPage::$permissions;

        return view(
            'admin.cms_pages.create', 
            compact('parent_options', 'permission_options')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \CounsellorsUK\Http\Requests\CmsPageRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CmsPageRequest $request)
    {
        CmsPage::create($request->all());

        return redirect()->route('admin.cms_page.index');
    }

    /**
     * Display the specified resource.
     *
     * @param \CounsellorsUK\Models\CmsPage $cms_page
     *
     * @return \Illuminate\Http\Response
     */
    public function show(CmsPage $cms_page)
    {
        return abort(501, 'Not implemented');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \CounsellorsUK\Models\CmsPage $cms_page
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(CmsPage $cms_page)
    {
        $parent_options = CmsPage::getOptionsForSelect2();
        $permission_options = CmsPage::$permissions;

        return view(
            'admin.cms_pages.edit', 
            compact('cms_page', 'parent_options', 'permission_options')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \CounsellorsUK\Http\Requests\CmsPageRequest $request
     * @param \CounsellorsUK\Models\CmsPage               $cms_page
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CmsPageRequest $request, CmsPage $cms_page)
    {
        try {
            $cms_page->fill($request->all())->save();
        } catch (LogicException $e) {
            Notification::error('Parent cannot be this page, or a descendant of it');

            return redirect()->back();
        }

        return redirect()->route('admin.cms_page.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \CounsellorsUK\Models\CmsPage $cms_page
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(CmsPage $cms_page)
    {
        $cms_page->delete();

        return redirect()->route('admin.cms_page.index');
    }

    /**
     * Move the resource up in the nested set.
     *
     * @param \CounsellorsUK\Models\CmsPage $cms_page
     *
     * @return \Illuminate\Http\Response
     */
    public function moveUp(CmsPage $cms_page)
    {
        $cms_page->up();

        return redirect()->route('admin.cms_page.index');
    }

    /**
     * Move the resource down in the nested set.
     *
     * @param \CounsellorsUK\Models\CmsPage $cms_page
     *
     * @return \Illuminate\Http\Response
     */
    public function moveDown(CmsPage $cms_page)
    {
        $cms_page->down();

        return redirect()->route('admin.cms_page.index');
    }
}
