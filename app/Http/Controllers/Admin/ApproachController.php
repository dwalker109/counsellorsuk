<?php

namespace CounsellorsUK\Http\Controllers\Admin;

use CounsellorsUK\Models\Approach;

class ApproachController extends SupportItemsController
{
    /**
     * Initialise a new approach controller instance.
     */
    public function __construct(Approach $approach)
    {
        $this->class = $approach;
        parent::__construct();
    }
}
