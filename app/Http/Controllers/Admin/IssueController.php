<?php

namespace CounsellorsUK\Http\Controllers\Admin;

use CounsellorsUK\Models\Issue;

class IssueController extends SupportItemsController
{
    /**
     * Initialise a new issue controller instance.
     */
    public function __construct(Issue $issue)
    {
        $this->class = $issue;
        parent::__construct();
    }
}
