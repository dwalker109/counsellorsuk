<?php

namespace CounsellorsUK\Http\Controllers\Admin;

use Carbon\Carbon;
use CounsellorsUK\Http\Controllers\Controller;
use CounsellorsUK\Http\Requests;
use CounsellorsUK\Models\Profile;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class HomeController extends Controller
{
    /**
     * Display the home view
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stats = collect([
            'live' => Profile::count(),
            'paid' => Profile::where('meta_is_subscriber', true)->count(),
            'new' => Profile::where('created_at', '>', Carbon::now()->subDays(30))->count(),
            'new_paid' => Profile::where('meta_is_subscriber', true)
                ->where('created_at', '>', Carbon::now()->subDays(30))->count(),
        ]);

        return view('admin.home', compact('stats'));
    }

    /**
     * Download all profiles data
     *
     * @return \Illuminate\Http\Response
     */
    public function exportProfiles()
    {
        $data =Profile::withoutGlobalScopes()->selectRaw('users.email, users.name, profiles.*')
            ->join('users', 'users.id', 'profiles.user_id')
            ->get();

        Excel::create('cuk_export_' . Carbon::now()->toDateString(), function ($excel) use ($data) {
            $excel->sheet('Sheetname', function ($sheet) use ($data) {
                $sheet->with($data);
            });
        })->download('csv');
    }
}
