<?php

namespace CounsellorsUK\Http\Controllers\Auth;

use Auth;
use Cache;
use Carbon\Carbon;
use CounsellorsUK\Http\Controllers\Controller;
use CounsellorsUK\Http\Requests\MagicLinkRequest;
use CounsellorsUK\Mail\MagicLinkMail;
use CounsellorsUK\Models\Profile;
use CounsellorsUK\Models\User;
use Hash;
use Illuminate\Http\RedirectResponse;
use Mail;
use Notification;
use Uuid;

class MagicLinkController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('throttle:5');
        $this->middleware('post_login', ['only' => 'process']);
    }

    /**
     * Send a magic login link.
     *
     * @param MagicLinkRequest $request
     * @return RedirectResponse
     */
    public function send(MagicLinkRequest $request)
    {
        $user = User::where('email', $request->get('magic_link_email'))->first();

        $user->refreshMagicLink();

        Cache::put(
            $short_link = Uuid::generate()->string,
            $user->magic_link_token,
            decrypt($user->magic_link_token)->get('expiry')
        );

        Mail::to($user)->queue(new MagicLinkMail($user, $short_link));

        Notification::success('Your magic link has been sent - please check your email.');

        return redirect()->route('home');
    }

    /**
     * Process a magic link login request.
     *
     * @param string $short_link
     * @return RedirectResponse
     */
    public function process($short_link)
    {
        $magic_link_token = Cache::pull($short_link);

        // Wrap all lookups and checking in try/catch to handle malformed
        // and malicious attempts - each check will throw an exception on failure
        try {
            $payload = decrypt($magic_link_token);

            $user = User::where('magic_link_token', $magic_link_token)->first();

            if (!$user) {
                Notification::error('Your magic link has already been used - please request another.');

                return redirect()->route('login');
            }

            if (! Hash::check($user->email, $payload->get('hash'))) {
                Notification::error('Your magic link was created for a different email address - please request another.');

                return redirect()->route('login');
            }

            if ($payload->get('expiry')->lt(Carbon::now())) {
                Notification::error('Your magic link has expired - please request another.');

                return redirect()->route('login');
            }
        } catch (\Exception $e) {
            abort(404);
        }

        $user->magic_link_token = null;
        $user->email_verified = true;
        $user->save();

        Auth::login($user, true);

        return redirect()->to($payload->get('intended'));
    }
}
