<?php

namespace CounsellorsUK\Http\Middleware;

use Closure;
use CounsellorsUK\Models\User;
use Hash;

class UpgradeInsecurePassword
{
    /**
     * Upgrade any sha1 encoded passwords from the V1 site before real auth.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::where('email', $request->email)->where('password', '')
            ->where('insecure_password', md5($request->password))
            ->first();
            
        if ($user) {
            $user->password = Hash::make($request->password);
            $user->insecure_password = null;
            $user->save();
        }
        
        return $next($request);
    }
}
