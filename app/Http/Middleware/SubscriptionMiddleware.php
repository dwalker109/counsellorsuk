<?php

namespace CounsellorsUK\Http\Middleware;

use Auth;
use Closure;

class SubscriptionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->subscription_override) {
            return redirect()->route('subscription.not_editable');
        }

        return $next($request);
    }
}
