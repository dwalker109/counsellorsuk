<?php

namespace CounsellorsUK\Http\Middleware;

use Auth;
use Closure;
use Carbon\Carbon;

class PostLogin
{
    /**
     * If login succeeded, update user's last login at attribute and ensure
     * they don't have an old insecure password attribute.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $user = Auth::user();
        
        if ($user) {
            $user->last_login_at = Carbon::now();
            $user->insecure_password = null;
            $user->save();
        }

        return $response;
    }
}
