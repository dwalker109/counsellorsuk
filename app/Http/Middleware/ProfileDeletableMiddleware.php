<?php

namespace CounsellorsUK\Http\Middleware;

use Auth;
use Closure;

class ProfileDeletableMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->profile->meta_is_subscriber) {
            return redirect()->route('profile.not_deletable');
        }

        return $next($request);
    }
}
