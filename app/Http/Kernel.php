<?php

namespace CounsellorsUK\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \CounsellorsUK\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \CounsellorsUK\Http\Middleware\VerifyCsrfToken::class,
            \Krucas\Notification\Middleware\NotificationMiddleware::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \CounsellorsUK\Http\Middleware\Authenticate::class,
        'auth.admin' => \CounsellorsUK\Http\Middleware\AuthenticateAdmin::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \CounsellorsUK\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'sub_editable' => \CounsellorsUK\Http\Middleware\SubscriptionMiddleware::class,
        'profile_deletable' => \CounsellorsUK\Http\Middleware\ProfileDeletableMiddleware::class,
        'insecure_upgrade' => \CounsellorsUK\Http\Middleware\UpgradeInsecurePassword::class,
        'post_login' => \CounsellorsUK\Http\Middleware\PostLogin::class,
    ];
}
