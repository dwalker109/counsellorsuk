<?php

namespace CounsellorsUK\Models;

use CounsellorsUK\Models\Profile;
use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    /**
     * The attributes which are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['tag', 'session', 'ip', 'referrer'];

    /**
     * Define relationship to profile.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }
}
