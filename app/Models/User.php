<?php

namespace CounsellorsUK\Models;

use Carbon\Carbon;
use CounsellorsUK\Models\Profile;
use Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Sofa\Eloquence\Eloquence;

class User extends Authenticatable
{
    use Notifiable;
    use Billable;

    use Eloquence;

    /**
     * Class constants for user signup sources
     */
    const SOURCE_STANDARD = 0;
    const SOURCE_SELECTED = 1;
    const SOURCE_IMPORTED = 2;

    /**
     * Eloquence\Searchable rules.
     *
     * @var array
     */
    protected $searchableColumns = ['name', 'email'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes which should be handled as dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'last_login_at', 'trial_ends_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'insecure_password', 'email_verify_token',
    ];

    /**
     * Define relationship to profile.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        // References to the user's profile *from* this model ignore the global scope,
        // so non-live user accounts can still log in to amend things
        return $this->hasOne(Profile::class)->withoutGlobalScope('live');
    }

    /**
     * Set a magic link token for this user.
     *
     * @return void
     */
    public function refreshMagicLink($intended = null, $expiry = null)
    {
        $intended = $intended ?? session()->pull('url.intended', route('profile.view'));
        
        if ($expiry instanceof Carbon) {
            $expiry = $expiry;
        } elseif ($expiry instanceof integer) {
            $expiry = Carbon::now()->addMinutes($expiry);
        } else {
            $expiry = Carbon::now()->addHour();
        }

        $payload = collect([
            'hash' => Hash::make($this->email),
            'expiry' => $expiry,
            'intended' => $intended,
        ]);

        $this->magic_link_token = encrypt($payload);
        $this->save();
    }
}
