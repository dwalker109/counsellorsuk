<?php

namespace CounsellorsUK\Models;

use CounsellorsUK\Models\Profile;
use Illuminate\Database\Eloquent\Model;

class BodyMembership extends Model
{
    /**
     * The attributes which are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['body', 'name', 'is_supervisory'];

    /**
     * Define relationship to profiles
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function profiles()
    {
        return $this->belongsToMany(Profile::class);
    }
}
