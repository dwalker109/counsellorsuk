<?php

namespace CounsellorsUK\Models;

use Auth;
use Cache;
use Converter;
use CounsellorsUK\Models\Address;
use CounsellorsUK\Models\Approach;
use CounsellorsUK\Models\BodyMembership;
use CounsellorsUK\Models\Issue;
use CounsellorsUK\Models\OtherService;
use CounsellorsUK\Models\Skill;
use CounsellorsUK\Models\Statistic;
use CounsellorsUK\Models\User;
use CounsellorsUK\Presenters\ProfilePresenter;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Image;
use Laracodes\Presenter\Traits\Presentable;
use Statistics;
use Storage;

/**
 * Class Profile.
 */
class Profile extends Model
{
    use Presentable;
    use Sluggable;

    /**
     * The attributes which are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'about',
        'appointments_at',
        'counselling_hours',
        'fees',
        'is_insured',
        'main_qualification',
        'phone_numbers',
        'probono_about',
        'probono_available',
        'registration_number',
        'bacp_certificate_number',
        'session_lengths',
        'social_media',
        'urls',
    ];

    /**
     * The variables to cast on save/load.
     *
     * @var array
     */
    protected $casts = [
        'appointments_at' => 'array',
        'counselling_hours' => 'array',
        'fees' => 'array',
        'is_approved' => 'boolean',
        'is_insured' => 'boolean',
        'is_live' => 'boolean',
        'verified_live_email_sent' => 'boolean',
        'phone_numbers' => 'array',
        'probono_available' => 'boolean',
        'session_lengths' => 'array',
        'social_media' => 'array',
        'urls' => 'array',
    ];

    /**
     * The presenter to use for this model.
     *
     * @var string
     */
    protected $presenter = ProfilePresenter::class;

    /**
     * Constants used to reference each profile stage.
     */
    const PHOTO = 1;
    const DETAILS = 2;
    const CONTACT = 3;
    const ADDRESSES = 4;
    const PRACTICE = 5;
    const COUNSELLING = 6;

    /**
     * Bit flags used to track profile completion.
     *
     * @var array
     */
    private static $stage_bitmasks = [
        self::PHOTO => 0b000001,
        self::DETAILS => 0b000010,
        self::CONTACT => 0b000100,
        self::ADDRESSES => 0b001000,
        self::PRACTICE => 0b010000,
        self::COUNSELLING => 0b100000,
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        // Always exclude non-live profiles
        static::addGlobalScope('live', function (Builder $builder) {
            $builder->where('is_live', true);
        });
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'user.name'
            ]
        ];
    }

    /**
     * Define relationship to user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Define relationship to approaches.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function approaches()
    {
        return $this->belongsToMany(Approach::class);
    }

    /**
     * Define relationship to issues covered.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function issues()
    {
        return $this->belongsToMany(Issue::class);
    }

    /**
     * Define relationship to other services.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function other_services()
    {
        return $this->belongsToMany(OtherService::class);
    }

    /**
     * Define relationship to skills.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function skills()
    {
        return $this->belongsToMany(Skill::class);
    }

    /**
     * Define relationship to professional body memberships.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function body_memberships()
    {
        return $this->belongsToMany(BodyMembership::class);
    }

    /**
     * Define relation to practice addresses.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    /**
     * Define relation to statistics.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function statistics()
    {
        return $this->hasMany(Statistic::class);
    }

    /**
     * Save the uploaded profile photo.
     *
     * @param \Illuminate\Http\Request
     */
    public function setProfileImage($request)
    {
        // Do nothing if no photo uploaded
        if ($request->hasFile('photo') === false) {
            return;
        }

        $photo_path_root = config('counsellorsuk.profile.photo_path_root');
        $photo = Image::make($request->file('photo'));

        // Original image
        Storage::put(
            "{$photo_path_root}/_original/{$this->id}",
            $photo->encode()
        );

        // Resized versions
        foreach (config('counsellorsuk.profile.photo_sizes') as $size => $dimensions) {
            Storage::put(
                "{$photo_path_root}/{$size}/{$this->id}",
                $photo->fit($dimensions['width'], $dimensions['height'])->encode()
            );
        }
    }

    /**
     * Delete the profile photos from the filesystem.
     */
    public function deleteProfileImage()
    {
        $photo_path_root = config('counsellorsuk.profile.photo_path_root');

        Storage::delete("{$photo_path_root}/_original/{$this->id}");

        foreach (config('counsellorsuk.profile.photo_sizes') as $size => $dimensions) {
            Storage::delete("{$photo_path_root}/{$size}/{$this->id}");
        }
    }

    /**
     * Check whether a profile stage is complete.
     *
     * @param int $stage
     *
     * @return bool
     */
    public function isStageComplete($stage)
    {
        return self::$stage_bitmasks[$stage] & $this->stage_bitmask;
    }

    /**
     * Mark a profile stage as complete.
     *
     * @param int $stage
     */
    public function markStageComplete($stage)
    {
        $this->stage_bitmask |= self::$stage_bitmasks[$stage];
        $this->save();
    }

    /**
     * Mark a profile stage as incomplete.
     *
     * @param int $stage
     */
    public function markStageIncomplete($stage)
    {
        $this->stage_bitmask = $this->stage_bitmask & (~ self::$stage_bitmasks[$stage]);
        $this->save();
    }

    /**
     * Add Eloquent attribute for each profile photo.
     *
     * @return stdClass
     */
    public function getPhotosAttribute()
    {
        $photo_path_root = config('counsellorsuk.profile.photo_path_root');

        // Add a data-url encoded string for each size, or use the not found image
        $images = [];
        foreach (config('counsellorsuk.profile.photo_sizes') as $size => $dimensions) {
            if (Storage::exists("{$photo_path_root}/{$size}/{$this->id}")) {
                $images[$size] = Image::make(
                    Storage::get("{$photo_path_root}/{$size}/{$this->id}")
                )->encode('data-url');
            } else {
                $images[$size] = Image::make(
                    config('counsellorsuk.profile.photo_not_found')
                )->fit($dimensions['width'], $dimensions['height'])->encode('data-url');
            }
        }

        return (object) $images;
    }

    /**
     * Add Eloquent attribute specifying whether the user has uploaded a photo.
     *
     * @return boolean
     */
    public function getHasPhotoAttribute()
    {
        $photo_path_root = config('counsellorsuk.profile.photo_path_root');

        return Storage::exists("{$photo_path_root}/_original/{$this->id}");
    }

    /**
     * Return the percentage profile completion.
     *
     * @return int
     */
    public function getCompletionPercentageAttribute()
    {
        $counter = 0;
        $stages = config('counsellorsuk.profile.stages');
        foreach ($stages as $stage) {
            $counter += $this->isStageComplete($stage->number) ? 1 : 0;
        }

        return $counter / count($stages) * 100;
    }

    /**
     * Return a Collection of info on profile stages
     *
     * @return Illuminate\Support\Collection
     */
    public function getStagesAttribute()
    {
        $all = collect(config('counsellorsuk.profile.stages'))->transform(function ($item, $key) {
            $item->complete = $this->isStageComplete($item->number);
            return $item;
        })->keyBy('number');

        $next = $all->first(function ($item) {
            return !$item->complete;
        });

        return (object) compact('all', 'next');
    }

    /**
     * Return info on profile views and actions
     *
     * @return Illuminate\Support\Collection
     */
    public function getPopularityAttribute()
    {
        /* Temporarily disabled */
        return (object) array_fill_keys(array_keys(Statistics::$tags), 'Disabled');

        // Will be recached regularly by a scheduled command, so cache forever
        return Cache::rememberForever("popularity.{$this->id}", function () {
            $popularity = [];

            foreach (Statistics::$tags as $tag => $description) {
                $popularity[$tag] = $this->statistics()->where('tag', $tag)
                    ->select('session')->distinct()->count('session');
            }

            return (object) $popularity;
        });
    }

    /**
     * Filter by location, constrained and ordered by distance radius.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param \CounsellorsUK\Model\Location      $location
     * @param int                                $distance
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeFilterLocation($query, $location, $distance)
    {
        // Param binding seems to fail due to the geocoding stuff in Location, so sanitise manually
        if (!$location instanceof Location) {
            return $query;
        }

        $st_distance_sphere_calc = "st_distance_sphere(locations.geo, POINT({$location->geo}))";

        // Initial distance searching
        $query->selectRaw("profiles.*, $st_distance_sphere_calc as distance")
            ->join('addresses', 'profiles.id', '=', 'addresses.profile_id')
            ->join('locations', 'addresses.location_id', '=', 'locations.id')
            ->whereRaw("$st_distance_sphere_calc <= ?", [
                Converter::from('length.mi')->to('length.m')->convert($distance)->getValue(),
            ]);

        // County searches should additionally return ALL profiles within that county
        if ($location->type === 'county') {
            $query->orWhereIn('location_id', $location->children()->pluck('id'));
        }

        // Postcode and town searches should include distance ordering
        if ($location->type === 'postcode' || $location->type === 'town') {
            $query->orderBy('distance', 'asc');
        }

        return $query;
    }

    /**
     * Filter by issue.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param \CounsellorsUK\Model\Issue|null    $issue
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeFilterIssue($query, $issue)
    {
        if ($issue === null) {
            return $query;
        }

        return $query = $query->whereHas('issues', function ($q) use ($issue) {
            $q->where('id', $issue->id);
        });
    }

    /**
     * Filter by approach.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param \CounsellorsUK\Model\Approach|null $approach
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeFilterApproach($query, $approach)
    {
        if ($approach === null) {
            return $query;
        }

        return $query = $query->whereHas('approaches', function ($q) use ($approach) {
            $q->where('id', $approach->id);
        });
    }
}
