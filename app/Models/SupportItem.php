<?php

namespace CounsellorsUK\Models;

use CounsellorsUK\Models\Profile;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Image;
use Sofa\Eloquence\Eloquence;
use Storage;

abstract class SupportItem extends Model
{
    use Eloquence, Sluggable
    {
        // Resolve trait collision
        Eloquence::replicate insteadof Sluggable;
    }

    /**
     * The attributes which are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['icon'];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'slug', 'name', 'icon'];

    /**
     * Eloquence\Searchable rules.
     *
     * @var array
     */
    protected $searchableColumns = ['name'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Define relationship to profiles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function profiles()
    {
        return $this->belongsToMany(Profile::class);
    }

    /**
     * Save the uploaded icon.
     *
     * @param \Illuminate\Http\Request
     */
    public function setIcon($request)
    {
        // Do nothing if no photo uploaded
        if ($request->hasFile('icon') === false) {
            return;
        }

        $icon_path_root = config('counsellorsuk.support_items.icon_path_root');
        $item_path = class_basename(get_class($this));
        $dimensions = config('counsellorsuk.support_items.icon_size');

        Storage::put(
            "{$icon_path_root}/{$item_path}/{$this->id}",
            Image::make($request->file('icon'))
                ->fit($dimensions['width'], $dimensions['height'])
                ->encode()
        );
    }

    /**
     * Add Eloquent attribute for the icon.
     *
     * @return string;
     */
    public function getIconAttribute()
    {
        $icon_path_root = config('counsellorsuk.support_items.icon_path_root');
        $item_path = class_basename(get_class($this));
        $dimensions = config('counsellorsuk.support_items.icon_size');

        // Return a data-url encoded string for the icon, or use the not found image
        if (Storage::exists("{$icon_path_root}/{$item_path}/{$this->id}")) {
            return Image::make(
                Storage::get("{$icon_path_root}/{$item_path}/{$this->id}")
            )->encode('data-url');
        } else {
            return Image::make(
                config('counsellorsuk.support_items.icon_not_found')
            )->fit($dimensions['width'], $dimensions['height'])->encode('data-url');
        }
    }

    /**
     * Return collated item data suitable for use in a templated Select2 control.
     *
     * @param string $value_field
     *
     * @return array
     */
    public static function getOptionsForSelect2($value_field = 'id')
    {
        // Return an array sorted and keyed by name, containing each identifier plus icon data
        return self::all()->sortBy('name')->keyBy('name')->map(function ($item, $key) use ($value_field) {
            return [
                'value' => $item->{$value_field},
                'data-icon' => $item->icon,
            ];
        });
    }
}
