<?php

namespace CounsellorsUK\Models;

use CounsellorsUK\Models\Location;
use CounsellorsUK\Models\Profile;
use CounsellorsUK\Presenters\AddressPresenter;
use Illuminate\Database\Eloquent\Model;
use Laracodes\Presenter\Traits\Presentable;

class Address extends Model
{
    use Presentable;

    /**
     * The attributes which are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'address_1',
        'address_2',
        'postal_code',
        'location_id',
        'is_primary',
        'show_full_address',
    ];

    /**
     * The variables to cast on save/load.
     *
     * @var array
     */
    protected $casts = [
        'is_primary' => 'boolean',
        'show_full_address',
    ];

    /**
     * The presenter to use for this model.
     *
     * @var string
     */
    protected $presenter = AddressPresenter::class;

    /**
     * Define relationship to profile.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }

    /**
     * Define relationship to location.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo(Location::class);
    }
}
