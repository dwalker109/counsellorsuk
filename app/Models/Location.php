<?php

namespace CounsellorsUK\Models;

use CounsellorsUK\Models\Address;
use CounsellorsUK\Models\Location;
use Cviebrock\EloquentSluggable\Sluggable;
use DB;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Location extends Model
{
    use Eloquence, Sluggable
    {
        // Resolve trait collision
        Eloquence::replicate insteadof Sluggable;
    }

    /**
     * The attributes which are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'geo',
        'type',
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'slug', 'name'];

    /**
     * Eloquence\Searchable rules.
     *
     * @var array
     */
    protected $searchableColumns = ['name'];

    /**
     * The fields which are of MySQL POINT type.
     *
     * @var array
     */
    protected $geofields = ['geo'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Define relationship to addresses.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    /**
     * Define relationship to parent location.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Location::class, 'parent_location_id');
    }

    /**
     * Define relationship to child locations.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Location::class, 'parent_location_id');
    }

    /**
     * Retrieve the name plus parent region, if possible
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->parent ? "{$this->attributes['name']}, {$this->parent->name}" : $this->attributes['name'];
    }

    /**
     * Cast a 'lat,lng' string to a MySQL POINT.
     *
     * @param string $value
     */
    public function setGeoAttribute($value)
    {
        $this->attributes['geo'] = DB::raw("POINT({$value})");
    }

    /**
     * Cast a MySQL POINT string [eg 'POINT(-10.23435 10.87654)'] to a simple 'lat,lng' string.
     *
     * @param string $value
     *
     * @return string
     */
    public function getGeoAttribute($value)
    {
        $value = substr($value, 6);
        $value = preg_replace('/[ ,]+/', ',', $value, 1);

        return substr($value, 0, -1);
    }

    /**
     * Retrieve the binary MySQL POINT data as plain text.
     *
     * @return string
     */
    public function newQuery()
    {
        $raw = '';
        foreach ($this->geofields as $column) {
            $raw .= " astext({$column}) as $column ";
        }

        return parent::newQuery()->addSelect('*', DB::raw($raw));
    }

    /**
     * Restrict to only counties.
     *
     * @param \Illuminate\Database\Query\Builder $query
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeCounties($query)
    {
        return $query->where('type', 'county');
    }

    /**
     * Restrict to only towns.
     *
     * @param \Illuminate\Database\Query\Builder $query
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeTowns($query)
    {
        return $query->where('type', 'town');
    }

    /**
     * Add a column for the distance from the specified point.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param \CounsellorsUK\Model\Location      $location
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeAddDistanceColumn($query, $location)
    {
        // Param binding seems to fail due to the geocoding stuff above, so sanitise manually
        if (!$location instanceof self) {
            return $query;
        }

        // Distance column is misleading against county searches - don't add it
        if ($location->type === 'county') {
            return $query;
        }

        return $query->selectRaw("st_distance_sphere(geo, POINT({$location->geo})) as distance");
    }
}
