<?php

namespace CounsellorsUK\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class CmsPage extends Model
{
    use NodeTrait;
    use Sluggable;

    // Page availability constants for view permissions
    const INHERIT = 'inherit';
    const ANYONE = 'anyone';
    const MEMBER = 'members';
    const SUBSCRIBER = 'subscribers';

    /**
     * List of page permissions
     *
     * @var array
     */
    public static $permissions = [
        self::INHERIT => 'Inherit permissions from parent page',
        self::ANYONE => 'Available to the general public',
        self::MEMBER => 'Available to all (free and paid) site members',
        self::SUBSCRIBER => 'Only available to paying subscribers',
    ];

    /**
     * The attributes which are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['parent_id', 'title', 'body', 'meta_description', 'permission'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * Return collated item data suitable for use in a templated Select2 control.
     *
     * @return array
     */
    public static function getOptionsForSelect2()
    {
        // Retrieve and transform each node with a prefix by depth, and prepend a top level node
        return self::withDepth()->defaultOrder()->get()->toFlatTree()->transform(function ($item, $key) {
            $item->title = str_repeat('&rightarrow;', $item->depth)." $item->title";

            return $item;
        })->pluck('title', 'id')->prepend('None (top level page)', 0);
    }

    /**
     * Calculate applied permission for the page.
     *
     * @param string $value
     * @return string
     */
    public function getAppliedPermissionAttribute()
    {
        if ($this->permission != SELF::INHERIT) {
            return $this->permission;
        }

        if (!$this->parent) {
            return self::ANYONE;
        }

        return $this->parent->applied_permission;
    }
}
