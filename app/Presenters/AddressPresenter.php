<?php

namespace CounsellorsUK\Presenters;

use Converter;
use Laracodes\Presenter\Presenter;

class AddressPresenter extends Presenter
{
    /**
     * Display all address components in a block.
     *
     * @return string
     */
    public function addressBlock()
    {
        if (!$this->model->show_full_address) {
            return "{$this->model->location->name}, United Kingdom";
        }

        return collect([
            $this->model->name,
            $this->model->address_1,
            $this->model->address_2,
            $this->model->location->name,
            $this->model->postal_code,
        ])->filter(function ($value) {
            return $value != false;
        })->unique()->implode('<br>');
    }
    
    /**
     * Display simplified address components as a simple search string.
     *
     * @return string
     */
    public function addressLookup()
    {
        if (!$this->model->show_full_address) {
            return $this->addressBlock();
        }
        
        return collect([
            $this->model->address_1,
            $this->model->address_2,
            $this->model->location->name,
            $this->model->postal_code,
        ])->filter(function ($value) {
            return $value != false;
        })->unique()->implode(', ');
    }

    /**
     * Display the distance (calculated from the Location during some queries) in miles.
     *
     * @return string
     */
    public function locationDistance()
    {
        if ($this->model->location->distance === null) {
            return null;
        }
        
        $distance = (integer) round(
            Converter::from('length.m')->to('length.mi')->convert($this->model->location->distance)->getValue()
        );
         
        return $distance === 0 ? null : "({$distance} miles away)";
    }
}
