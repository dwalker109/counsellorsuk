<?php

namespace CounsellorsUK\Presenters;

use Converter;
use Laracodes\Presenter\Presenter;
use Purifier;

class ProfilePresenter extends Presenter
{
    /**
     * Display about me information, truncated for free profiles.
     *
     * @return string
     */
    public function about()
    {
        // Add 10% to the limit to account for tags
        $limit = config('counsellorsuk.free_profile_about_limit') * 1.1;

        $about = $this->model->meta_is_subscriber
            ? $this->model->about
            : str_limit($this->model->about, $limit);
            
        return Purifier::clean($about);
    }
    
    /**
     * Display account age.
     *
     * @return string
     */
    public function age()
    {
        return $this->model->created_at->diffForHumans();
    }

    /**
     * Display professional body memberships.
     *
     * @return string
     */
    public function bodyMemberships()
    {
        return $this->model->body_memberships->implode('body', ', ');
    }

    /**
     * Display full professional body memberships.
     *
     * @return string
     */
    public function bodyMembershipsFull()
    {
        return $this->model->body_memberships->implode('name', ', ');
    }

    /**
     * Filter and display phone numbers.
     *
     * @return Collection
     */
    public function phoneNumbers()
    {
        return collect($this->model->phone_numbers)->filter(function ($item, $key) {
            return $item != false;
        })->transform(function ($item, $key) {
            return phone_format($item, 'GB');
        });
    }

    /**
     * Display locations string.
     */
    public function locations()
    {
        return $this->model->addresses->map(function ($item) {
            return $item->location->name;
        })->implode(' and ');
    }

    /**
     * Filter and display URLs.
     *
     * @return stdClass
     */
    public function urls()
    {
        return collect($this->model->urls)->filter(function ($item, $key) {
            return $item != false;
        })->transform(function ($item, $key) {
            return (object) [
                'href' => parse_url($item, PHP_URL_SCHEME) === null ? "http://{$item}" : $item,
                'text' => str_limit(
                    preg_replace(["/http:\/\//", "/https:\/\//", "/www./"], null, $item),
                    20
                ),
            ];
        });
    }

    /**
     * Display listing of issues worked with.
     *
     * @return string
     */
    public function issues()
    {
        return $this->model->issues->implode('name', ', ');
    }

    /**
     * Display counselling hours by years.
     *
     * @return string
     */
    public function counsellingHours()
    {
        // Neither specified, return null
        if (!$this->model->counselling_hours['total'] && !$this->model->counselling_hours['years']) {
            return false;
        }

        // Years not specified, return hours only
        if (!$this->model->counselling_hours['years']) {
            return "{$this->model->counselling_hours['total']} hours";
        }

        // Both specified
        return "{$this->model->counselling_hours['total']} hours over 
            {$this->model->counselling_hours['years']} years";
    }

    /**
     * Prepare fees data for display.
     *
     * @return array
     */
    public function fees()
    {
        return collect($this->model->fees)->filter(function ($item, $key) {
            return $item != false;
        })->transform(function ($item, $key) {
            return (object) [
                'type' => ucfirst($key),
                'price' => Converter::to('currency.gbp')->value((float) $item)->format(),
                'tooltip' => config("counsellorsuk.profile.fees.{$key}"),
            ];
        });
    }

    /**
     * Prepare session lengths data for display.
     *
     * @return array
     */
    public function sessionLengths()
    {
        return collect($this->model->session_lengths)->filter(function ($item, $key) {
            return $item != false;
        })->transform(function ($item, $key) {
            return (object) [
                'type' => ucfirst($key),
                'length' => $item,
            ];
        });
    }

    /**
     * Prepare appointment data for display.
     *
     * @return array
     */
    public function appointmentsAt()
    {
        return collect($this->model->appointments_at)->filter(function ($item, $key) {
            return $item != false;
        })->transform(function ($item, $key) {
            return ucfirst(str_replace('_', ' ', $key));
        });
    }
}
