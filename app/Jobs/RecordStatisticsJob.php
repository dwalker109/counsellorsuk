<?php

namespace CounsellorsUK\Jobs;

use CounsellorsUK\Jobs\Job;
use CounsellorsUK\Models\Profile;
use CounsellorsUK\Models\Statistic;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RecordStatisticsJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * User profile to record the statistic against
     *
     * @var $profile
     */
    protected $profile;
    
    /**
     * Raw statistic data to record
     *
     * @var $raw_data
     */
    protected $raw_data;
    
    /**
     * Create a new job instance.
     *
     * @param CounsellorsUK\Models\Profile $profile
     * @param array $raw_data
     * @return void
     */
    public function __construct(Profile $profile, $raw_data)
    {
        $this->profile = $profile;
        $this->raw_data = $raw_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $statistic = new Statistic($this->raw_data);
        $this->profile->statistics()->save($statistic);
    }
}
