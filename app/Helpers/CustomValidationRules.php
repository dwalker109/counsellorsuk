<?php

namespace CounsellorsUK\Helpers;

use Auth;
use Hash;

class CustomValidationRules
{
    /**
     * Fail if json fields are sent spoofed keys.
     *
     * @param string                          $attribute
     * @param array                           $value
     * @param array                           $parameters
     * @param Illuminate\Validation\Validator $validator
     *
     * @return integer
     */
    public function enforceJsonStructure($attribute, $value, $parameters, $validator)
    {
        return count(array_except($value, $parameters)) === 0 ? true : abort(400, 'Bad request');
    }
    
    /**
     * Check if the value matches the password of the current user.
     *
     * @param string                          $attribute
     * @param array                           $value
     * @param array                           $parameters
     * @param Illuminate\Validation\Validator $validator
     *
     * @return boolean
     */
    public function isCurrentPassword($attribute, $value, $parameters, $validator)
    {
        return Hash::check($value, Auth::user()->password);
    }
}
