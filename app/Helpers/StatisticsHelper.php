<?php

namespace CounsellorsUK\Helpers;

use Agent;
use CounsellorsUK\Jobs\RecordStatisticsJob;
use CounsellorsUK\Models\Profile;

class StatisticsHelper
{
    // Tag constants available
    const PROFILE_VIEWED = 'profile_viewed';
    const PROFILE_SEARCH_RESULTS = 'profile_search_results';
    const PROFILE_CONTACTED = 'profile_contacted';
    const PROFILE_DELETED = 'profile_deleted';
    
    /**
     * List of tags to present in popularity property
     *
     * @var array
     */
    public static $tags = [
        self::PROFILE_VIEWED => 'Profile page was viewed',
        self::PROFILE_SEARCH_RESULTS => 'Profile appeared in active page of search results',
        self::PROFILE_CONTACTED => 'Counsellor was contacted',
    ];
    
    /**
     * Queue a job to record detailed info about the visitor and profile
     *
     * @param CounsellorsUK\Models\Profile $profile
     * @param string|null $tag
     */
    public static function record(Profile $profile, $tag = null)
    {
        if (Agent::isRobot()) {
            return;
        }
        
        $raw_data = [
            'tag' => $tag,
            'session' => session()->getId(),
            'ip' => request()->ip(),
            'referrer' => url()->previous(),
        ];

        dispatch(new RecordStatisticsJob($profile, $raw_data));
    }
}
