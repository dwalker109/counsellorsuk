<?php

namespace CounsellorsUK\Listeners;

use CounsellorsUK\Events\UserCreated;
use CounsellorsUK\Mail\NotifyMail;
use CounsellorsUK\Mail\WelcomeMail;
use CounsellorsUK\Mail\WelcomeSelectedMail;
use CounsellorsUK\Models\Profile;
use CounsellorsUK\Models\User;
use Mail;

class PrepareProfile
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserCreated $event
     */
    public function handle(UserCreated $event)
    {
        $user = $event->user;
        
        // Create a new Profile for the new user
        $profile = new Profile;

        // Further customise the new profile and send mails, based on signup source
        switch ($user->source) {
            
            /**
             * User signed up themselves
             */
            case User::SOURCE_STANDARD:
                // Send them a welcome email
                Mail::to($user)->queue(new WelcomeMail($user));
                
                // Notify us of the signup
                Mail::to(config('counsellorsuk.notification_email.address'))
                ->queue(new NotifyMail($user, [
                    'heading' => 'A new counsellor has signed up!', 
                    'subject' => 'CounsellorsUK signup notification'
                ]));
                
                break;

            /**
             * User was selected for an account and signed up by us
             */
            case User::SOURCE_SELECTED:
                // Set to verified and live
                $profile->is_verified = true;
                $profile->is_live = true;
                
                // Notify them after a delay
                Mail::to($user)->later(
                    config('counsellorsuk.selected_signup_welcome_delay'),
                    new WelcomeSelectedMail($user)
                );
                
                break;

            /**
             * User was imported
             */
            case User::SOURCE_IMPORTED:
                // Do nothing
                break;
        }
        
        // Save and associate the new profile
        $user->profile()->save($profile);
    }
}
