<?php

namespace CounsellorsUK\Listeners;

use Artisan;
use CounsellorsUK\Events\UserSubscribed;
use CounsellorsUK\Mail\NotifyMail;
use CounsellorsUK\Mail\SubscribedMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class ProcessSubscription
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserSubscribed  $event
     *
     * @return void
     */
    public function handle(UserSubscribed $event)
    {
        $user = $event->user;

        // Queue an update of profile meta columns
        Artisan::queue('profiles:rebuild-meta');

        // Queue a subscribed email to the user
        Mail::to($user)->queue(new SubscribedMail($user));
        
        // Notify us of the subscription
        Mail::to(config('counsellorsuk.notification_email.address'))
        ->queue(new NotifyMail($user, [
            'heading' => 'A counsellor has suscribed!', 
            'subject' => 'CounsellorsUK subscription notification'
        ]));
    }
}
