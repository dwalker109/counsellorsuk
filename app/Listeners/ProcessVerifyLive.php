<?php

namespace CounsellorsUK\Listeners;

use Artisan;
use CounsellorsUK\Events\ProfileSaving;
use CounsellorsUK\Mail\VerifiedLiveMail;
use CounsellorsUK\Mail\NotifyMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class ProcessVerifyLive
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProfileSaving  $event
     *
     * @return void
     */
    public function handle(ProfileSaving $event)
    {
        $profile = $event->profile;

        // Account already verified - do nothing
        if ($profile->verified_live_email_sent) {
            return;
        }

        // We have verified and set live - notify user
        if ($profile->is_verified && $profile->is_live) {
            Mail::to($profile->user)->queue(new VerifiedLiveMail($profile->user));
            
            // Mark as sent
            $profile->verified_live_email_sent = true;

            return;
        }

        // User has just changed one of the cert fields - notify us
        if ($profile->isDirty(['registration_number', 'bacp_certificate_number'])) {
            Mail::to(config('counsellorsuk.notification_email.address'))
                ->queue(new NotifyMail($profile->user, [
                    'heading' => 'A counsellor has added a registration and/or BACP certificate number!', 
                    'subject' => 'CounsellorsUK verification notification'
                ]));

            return;
        }
    }
}
