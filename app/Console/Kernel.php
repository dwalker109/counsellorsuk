<?php

namespace CounsellorsUK\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
        Commands\RebuildMetaProfiles::class,
        Commands\RebuildSortProfiles::class,
        Commands\PopularityCacheProfiles::class,
        Commands\FixUrlsInProfiles::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('profiles:rebuild-meta')
            ->everyMinute()
            ->withoutOverlapping();

        $schedule->command('profiles:rebuild-sort')
            ->dailyAt('01:00');

        $schedule->command('profiles:popularity-cache')
            ->dailyAt('02:00');
    }
    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
