<?php

namespace CounsellorsUK\Console\Commands;

use CounsellorsUK\Models\Profile;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Illuminate\Console\Command;

class FixUrlsInProfiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'profiles:fix-urls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ensures URLs are valid and reachable';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var Client $client */
        $client = new Client;

        $profiles = Profile::all();

        $bar = $this->output->createProgressBar($profiles->count());

        $profiles->each(function (Profile $profile) use ($client, $bar) {
            $urls = collect($profile->urls)->map(function ($url) use ($client) {
                $parsed_url = (object) parse_url($url);
                $url = ($parsed_url->scheme ?? 'http') . '://' . ($parsed_url->host ?? null) . ($parsed_url->path ?? null);

                try {
                    $request = $client->createRequest('HEAD', $url, ['connect_timeout' => 10]);
                    $response = $client->send($request);

                    return $response->getStatusCode() === 200 ? $response->getEffectiveUrl() : (string) null;
                } catch (\Exception $e) {
                    return (string) null;
                }
            });

            $profile->urls = $urls;
            $profile->save();

            $bar->advance();
        });

        $bar->finish();
    }
}
