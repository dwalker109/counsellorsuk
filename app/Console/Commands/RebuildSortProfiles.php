<?php

namespace CounsellorsUK\Console\Commands;

use CounsellorsUK\Models\Profile;
use Illuminate\Console\Command;

class RebuildSortProfiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'profiles:rebuild-sort';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refreshes rotating sort column for all profiles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (Profile::inRandomOrder()->cursor() as $index => $profile) {
            $profile->rotating_sort = $index;
            $profile->save();
        }
    }
}