<?php

namespace CounsellorsUK\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RebuildMetaProfiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'profiles:rebuild-meta';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refreshes meta columns for all profiles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::transaction(function () {
            // Remove expired/cancelled subscriptions
            DB::table('subscriptions')
                ->whereNotNull('subscriptions.ends_at')
                ->where('subscriptions.ends_at', '<', Carbon::now())
                ->delete();
            
            // Paid subscriptions
            DB::table('profiles')
                ->join('subscriptions', 'profiles.user_id', '=', 'subscriptions.user_id')
                ->update(['profiles.meta_is_subscriber' => true]);
            
            // No paid subscription
            DB::table('profiles')
                ->leftJoin('subscriptions', 'profiles.user_id', '=', 'subscriptions.user_id')
                ->whereNull('subscriptions.user_id')
                ->update(['profiles.meta_is_subscriber' => false]);
                
            // Overriden subscriptions
            DB::table('profiles')
                ->join('users', 'profiles.user_id', '=', 'users.id')
                ->where('users.subscription_override', true)
                ->update(['profiles.meta_is_subscriber' => true]);
        });
    }
}
