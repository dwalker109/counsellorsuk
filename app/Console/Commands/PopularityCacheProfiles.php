<?php

namespace CounsellorsUK\Console\Commands;

use Cache;
use CounsellorsUK\Models\Profile;
use Illuminate\Console\Command;

class PopularityCacheProfiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'profiles:popularity-cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rebuild the cached popularity attribute on each profile.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (Profile::orderBy('created_at', 'DESC')->cursor() as $profile) {
            Cache::forget("popularity.{$profile->id}");
            $profile->popularity; // Accessing this property will recache it
        }
    }
}
