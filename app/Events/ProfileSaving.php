<?php

namespace CounsellorsUK\Events;

use CounsellorsUK\Models\Profile;
use Illuminate\Queue\SerializesModels;

class ProfileSaving extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     */
    public function __construct(Profile $profile)
    {
        $this->profile = $profile;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
