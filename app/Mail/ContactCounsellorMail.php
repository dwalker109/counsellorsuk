<?php

namespace CounsellorsUK\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactCounsellorMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The form data.
     *
     * @var stdClass
     */
    public $info;
    
    /**
     * Whether this is a followup email.
     *
     * @var boolean
     */
    public $is_followup;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $info, $is_followup = false)
    {
        $this->info = (object) $info;
        $this->is_followup = $is_followup;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->info->email) {
            $this->replyTo($this->info->email);
        }
        
        if (!$this->is_followup) {
            $this->view('emails.contact_counsellor')
                ->subject('You have been contacted by a potential client via CounsellorsUK');
        } else {
            $this->view('emails.contact_counsellor_followup')
                ->subject('Reminder: You were recently contacted by a potential client via CounsellorsUK');
        }

        return $this;
    }
}
