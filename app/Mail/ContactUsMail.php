<?php

namespace CounsellorsUK\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactUsMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The form data.
     *
     * @var stdClass
     */
    public $info;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $info)
    {
        $this->info = (object) $info;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->info->email) {
            $this->replyTo($this->info->email);
        }
        
        return $this->view('emails.contact_us')
            ->subject('Contact form sent via CounsellorsUK website');
    }
}
