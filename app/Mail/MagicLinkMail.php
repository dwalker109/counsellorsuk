<?php

namespace CounsellorsUK\Mail;

use CounsellorsUK\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MagicLinkMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user to login.
     *
     * @var User
     */
    public $user;

    /**
     * Cached short link to use for URL
     */
    public $short_link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, string $short_link)
    {
        $this->user = $user;
        $this->short_link = $short_link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.magic_link')
            ->subject('Your magic login link for CounsellorsUK');
    }
}
