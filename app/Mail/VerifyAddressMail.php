<?php

namespace CounsellorsUK\Mail;

use CounsellorsUK\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifyAddressMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user to verify.
     *
     * @var User
     */
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.verify_email')
            ->subject('Please verify your email address for CounsellorsUK');
    }
}
