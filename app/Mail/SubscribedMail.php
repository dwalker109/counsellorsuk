<?php

namespace CounsellorsUK\Mail;

use CounsellorsUK\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscribedMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user to welcome.
     *
     * @var User
     */
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.subscribed')
            ->subject('Thank you for subscribing to CounsellorsUK');
    }
}
