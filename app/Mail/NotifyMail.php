<?php

namespace CounsellorsUK\Mail;

use CounsellorsUK\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user this notification is regarding.
     *
     * @var User
     */
    public $user;
    
    /**
     * The heading for this email
     *
     * @var string
     */
    public $heading;
    
    /**
     * The subject for this email
     *
     * @var string
     */
    protected $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $details = [])
    {
        $this->user = $user;
        $this->heading = $details['heading'] ?? 'User details';
        $this->subject = $details['subject'] ?? 'CounsellorsUK notification';;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.notify');
    }
}
